#!/usr/bin/env python
import sys,os
import time
import argparse

import numpy as np
import networkx as nx

def doArgs(argList, name):
    parser = argparse.ArgumentParser(description=name)

    parser.add_argument('-v', "--verbose", action="store_true", help="Enable verbose debugging", default=False)
    parser.add_argument('--input', action="store", dest="inputFn", type=str, help="Input file name", required=True)
    parser.add_argument('--output', action="store", dest="outputFn", type=str, help="Output dir name", required=True)

    return parser.parse_args(argList)

def main():
    progName = "Convert OD Matrix"
    args = doArgs(sys.argv[1:], progName)

    verbose = args.verbose
    inputFn = args.inputFn
    outputFn = args.outputFn

    print "Starting %s" % (progName)
    startTime = float(time.time())

    if not os.path.isfile(inputFn):
        print "Input doesn't exist, exiting"
        return

    outputBase = os.path.dirname(outputFn)
    if outputBase!='' and not os.path.exists(outputBase):
        print "Output directory doesn't exist, making output dirs: %s" % (outputBase)
        os.makedirs(outputBase)


    data = np.loadtxt(inputFn, dtype=int, delimiter=",", skiprows=0)

    keys = data[0,:]

    countyOutPath = os.path.join(outputBase,"county_outflows/")
    if not os.path.exists(countyOutPath):
        os.makedirs(countyOutPath)

    countyInPath = os.path.join(outputBase,"county_inflows/")
    if not os.path.exists(countyInPath):
        os.makedirs(countyInPath)

    for i,key in enumerate(keys):
        countyIn = open(os.path.join(countyInPath,"%s.csv"%(key)),"w")
        countyOut = open(os.path.join(countyOutPath,"%s.csv"%(key)),"w")

        countyIn.write("id,commuters\n")
        countyOut.write("id,commuters\n")

        for j,key2 in enumerate(keys):
            countyIn.write("%s,%d\n" % (key2,data[i+1][j]))
            countyOut.write("%s,%d\n" % (key2,data[j+1][i]))

        countyIn.close()
        countyOut.close()


    print "Finished in %0.4f seconds" % (time.time() - startTime)
    return

if __name__ == '__main__':
    #sys.argv = ["programName.py","--input","test.txt","--output","tmp/test.txt"]
    main()

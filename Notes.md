## Data Download

To estimate sea level rise we need the current elevation of the coastline.


http://wiki.openstreetmap.org/wiki/SRTM#Data_in_OSM_format_.28XML.29

http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/North_America/
http://e4ftl01.cr.usgs.gov/SRTM/SRTMGL3.003/2000.02.11/



wget -r -l1 -H -t1 -nd -N -np -A.zip -erobots=off http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/North_America/
wget -r -l1 -H -t1 -nd -N -np -A.jpg -erobots=off http://e4ftl01.cr.usgs.gov/SRTM/SRTMGL3.003/2000.02.11/



https://coast.noaa.gov/htdata/Inundation/SLR/SLRdata/FL/ALFL_MOB_TLH_slr_data_dist.zip

https://coast.noaa.gov/htdata/Inundation/SLR/SLRdata/FL/ALFL_MOB_TLH_dems.zip



https://lpdaac.usgs.gov/dataset_discovery/measures/measures_products_table/srtmgl30_v021

http://e4ftl01.cr.usgs.gov/SRTM/SRTMGL30.002/2000.02.11/

http://www.asprs.org/a/publications/pers/2006journal/march/2006_mar_249-260.pdf

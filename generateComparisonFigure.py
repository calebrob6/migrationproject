#!/usr/bin/env python
import sys,os
import time
import argparse

import numpy as np
from scipy import stats

import matplotlib.pyplot as plt

def doArgs(argList, name):
    parser = argparse.ArgumentParser(description=name)

    parser.add_argument('-v', "--verbose", action="store_true", help="Enable verbose debugging", default=False)
    parser.add_argument('--input1', action="store", dest="inputFn1", type=str, help="Input Ground Truth file name", required=True)
    parser.add_argument('--input2', action="store", dest="inputFn2", type=str, help="Input Radiation Output file name", required=True)

    return parser.parse_args(argList)

def main():
    progName = "Gen Paper Figures"
    args = doArgs(sys.argv[1:], progName)

    verbose = args.verbose
    inputFn1 = args.inputFn1
    inputFn2 = args.inputFn2

    print "Starting %s" % (progName)
    startTime = float(time.time())

    if not os.path.isfile(inputFn1) or not os.path.isfile(inputFn2):
        print "Input doesn't exist, exiting"
        return

    print "Loading input files"
    data1 = np.loadtxt(inputFn1, dtype=int, delimiter=",", skiprows=1)
    data2 = np.loadtxt(inputFn2, dtype=int, delimiter=",", skiprows=1)

    for i in xrange(data1.shape[0]):
        data1[i,i] = 0
        data2[i,i] = 0

    data1 = data1.flatten()
    data2 = data2.flatten()


    pts1 = []
    pts2 = []
    for i in xrange(data1.shape[0]):
        if data1[i]!=0 and data2[i]!=0:
        #if data1[i]!=0 and data2[i]>=10:
            pts1.append(data1[i])
            pts2.append(data2[i])

    NUMBINS = 11
    MIN, MAX = np.min(pts1),np.max(pts1)
    hist, bin_edges = np.histogram(pts1, bins = 10 ** np.linspace(np.log10(MIN), np.log10(MAX), NUMBINS))
    print hist

    binMids = np.array([0 for i in range(hist.shape[0])])
    bins = [[] for i in range(hist.shape[0])]
    for i in range(0,bin_edges.shape[0]-1):
        lowVal = bin_edges[i]
        highVal = bin_edges[i+1]
        for j in xrange(len(pts1)):
            xVal = pts1[j]
            yVal = pts2[j]
            if lowVal<=xVal<highVal:
                bins[i].append(yVal)

        binMids[i] = (highVal+lowVal)//2

    binMeans = [np.mean(b) for b in bins]
    binErrors = [np.std(b) for b in bins]

    bins = [np.array(b) for b in bins]
    logBins = [np.log10(b) for b in bins]

    for b in bins:
        print b.shape

    fig, ax = plt.subplots(figsize=(10, 10))



    boxprops = dict(linestyle='-', linewidth=0, color='green')
    flierprops = dict(marker='o', markerfacecolor='green', markersize=12, linestyle='none')
    medianprops = dict(linestyle='-', linewidth=2.5, color='black')
    meanpointprops = dict(marker='o', markeredgecolor='black', markerfacecolor='black')
    capprops = dict(linestyle='-', linewidth=2.5, color='black')
    whiskerprops = dict(linestyle='-', linewidth=2.5, color='black')

    #ax.errorbar(binMids, binMeans, yerr=binErrors)
    bp = ax.boxplot(bins, positions=binMids, widths=binMids/3, whis=[9,91], showmeans=True, showfliers=False, patch_artist=True,
                         flierprops=flierprops, medianprops=medianprops, meanprops=meanpointprops, boxprops=boxprops, capprops=capprops, whiskerprops=whiskerprops)

    for patch in bp['boxes']:
        #if patch.contains_point()
        xCoords = patch.get_path().vertices[:-1,0]
        yCoords = patch.get_path().vertices[:-1,1]


        print xCoords
        for x,y in patch.get_path().vertices:
            print x,y
        print " "
        patch.set_facecolor("green")
        patch.set_alpha(0.5)

    '''
    ax.set_xscale('log')
    ax.axis([1, 100000, 0, 5])
    ax.set_yticklabels([r"$10^{%d}$" % a for a in np.arange(0, 5)])
    xs = np.linspace(1,100000)
    ys = np.log10(xs)
    ax.plot(xs, ys, 'k-', alpha=1.0, linewidth=2.0)
    ax.scatter(pts1,np.log10(pts2),s=2,color='grey',alpha=0.6)
    '''

    ax.set_ylabel('Migrants (Data)', size=20)
    ax.set_xlabel('Migrants (Radiation)', size=20)
    ax.set_title('Radiation Output vs. Ground Truth', size=28)
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.axis([1, 100000, 1, 100000])

    xs = np.linspace(1,100000)
    ax.plot(xs, xs, 'k-', alpha=1.0, linewidth=2.0, zorder=1)

    ax.scatter(pts1,pts2,s=2,color='grey',alpha=0.45, zorder=0)


    plt.show()

    print "Finished in %0.4f seconds" % (time.time() - startTime)
    return

if __name__ == '__main__':
    sys.argv = ["programName.py","--input1","scenarioData/default0.3_output_2007.csv","--input2","scenarioData/lowerFlorida_output_2007.csv"]
    main()

import fiona
import rasterio
from rasterio.tools.mask import mask
import numpy as np
import pandas as pd

for x in range(0,11):
    print x
    src = rasterio.open("usRaster.tif")
    data = src.read()
    profile = src.profile
    src.close()

    data = data.squeeze()

    newMask = np.load("outputs/mask_%d.npy" % (x))
    newMask = newMask.reshape(1,newMask.shape[0],newMask.shape[1])
    newMask = newMask.astype(np.float32)

    with rasterio.open("outputs/mask_%d.tif" % (x), 'w', **profile) as dst:
        dst.write(newMask)

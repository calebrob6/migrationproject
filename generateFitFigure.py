#!/usr/bin/env python
import sys,os
import time
import argparse

import numpy as np
from scipy import stats

import matplotlib.pyplot as plt

from Data import *

import pandas as pd
import sklearn.metrics

def doArgs(argList, name):
    parser = argparse.ArgumentParser(description=name)

    parser.add_argument('-v', "--verbose", action="store_true", help="Enable verbose debugging", default=False)
    parser.add_argument('--inputPop', action="store", dest="inputFn1", type=str, help="Input Population file name", required=True)
    parser.add_argument('--inputMatrix', action="store", dest="inputFn2", type=str, help="Input Migration Matrix Output file name", required=True)
    parser.add_argument('--year', action="store", dest="year", type=int, help="Year for title", required=True)

    return parser.parse_args(argList)

def main():
    progName = "Gen Paper Figures"
    args = doArgs(sys.argv[1:], progName)

    verbose = args.verbose
    inputFn1 = args.inputFn1
    inputFn2 = args.inputFn2
    year = args.year

    print "Starting %s" % (progName)
    startTime = float(time.time())

    if not os.path.isfile(inputFn1) or not os.path.isfile(inputFn2):
        print "Input doesn't exist, exiting"
        return

    print "Loading input files"
    data1 = loadPopulationFile(inputFn1)
    data2 = np.loadtxt(inputFn2, dtype=int, delimiter=",", skiprows=1)
    for i in range(data2.shape[0]):
        data2[i,i] = 0

    xVals = []
    scatterYVals1 = []
    pts1 = []
    for i in xrange(len(data1)):
        xVals.append(data1[i])
        yVal = np.sum(data2[i,:])

        pts1.append((data1[i],yVal))
        scatterYVals1.append(yVal)

    pts1 = sorted(pts1,key=lambda x:x[0])


    NUMBINS = 30
    MIN, MAX = np.min(xVals),np.max(xVals)
    #hist, bin_edges = np.histogram(pts1, bins = 10 ** np.linspace(np.log10(MIN), np.log10(MAX), NUMBINS))
    hist, bin_edges = np.histogram(xVals, bins = np.logspace(np.log10(MIN), np.log10(MAX), NUMBINS))

    binMids = np.array([0 for i in range(hist.shape[0])])
    bins = [[] for i in range(hist.shape[0])]
    for i in range(0,bin_edges.shape[0]-1):
        lowVal = bin_edges[i]
        highVal = bin_edges[i+1]
        for j in xrange(len(pts1)):
            xVal = pts1[j][0]
            yVal = pts1[j][1]
            if lowVal<=xVal<highVal:
                bins[i].append(yVal)

        binMids[i] = (highVal+lowVal)//2

    #binMeans = [np.mean(b) if len(b)>0 else 0 for b in bins if len(b)>0]
    #inErrors = [np.std(b) for b in bins if len(b)>0]

    binMeans = []
    binErrors = []
    for b in bins:
        if len(b) > 0:
            binMeans.append(np.mean(b))
            binErrors.append(np.std(b))
        else:
            binMeans.append(0)
            binErrors.append(0)

    bins = [np.array(b) for b in bins]


    fig, ax = plt.subplots(figsize=(10, 10))

    '''
    boxprops = dict(linestyle='-', linewidth=0, color='green')
    flierprops = dict(marker='o', markerfacecolor='green', markersize=12, linestyle='none')
    medianprops = dict(linestyle='-', linewidth=2.5, color='black')
    meanpointprops = dict(marker='o', markeredgecolor='black', markerfacecolor='black')
    capprops = dict(linestyle='-', linewidth=2.5, color='black')
    whiskerprops = dict(linestyle='-', linewidth=2.5, color='black')
    bp = ax.boxplot(bins, positions=binMids, widths=binMids/3, whis=[9,91], showmeans=True, showfliers=False, patch_artist=True,
                         flierprops=flierprops, medianprops=medianprops, meanprops=meanpointprops, boxprops=boxprops, capprops=capprops, whiskerprops=whiskerprops)
    for patch in bp['boxes']:
        #if patch.contains_point()
        xCoords = patch.get_path().vertices[:-1,0]
        yCoords = patch.get_path().vertices[:-1,1]

        print xCoords
        for x,y in patch.get_path().vertices:
            print x,y
        print " "
        patch.set_facecolor("green")
        patch.set_alpha(0.5)
    '''

    ax.set_ylabel('Migrants, $T_i$', size=20)
    ax.set_xlabel('Population, $m_i$', size=20)
    ax.set_title('Number of outgoing migrants from IRS data vs. Population, %d' % (year), size=20)
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.axis([1e2, 1e8, 1e0, 1e6])

    xVals = np.array(xVals)
    yVals = np.array(scatterYVals1)

    xVals = xVals[yVals!=0]
    yVals = yVals[yVals!=0]

    '''
    slope, intercept, r_value, p_value, std_err = stats.linregress(xVals,yVals)
    xs = np.linspace(1e2,1e8,num=100000)
    ys = xs*slope
    ax.plot(xs,ys)
    print "Slope from linear fit: %0.4f" % (slope)
    slope = slope
    '''

    r2Score = sklearn.metrics.r2_score
    explainedVarianceScore = sklearn.metrics.explained_variance_score
    maeScore = sklearn.metrics.mean_absolute_error
    mseScore = sklearn.metrics.mean_squared_error
    medianaeScore = sklearn.metrics.median_absolute_error

    xLog = np.log10(xVals)
    yLog = np.log10(yVals)
    coeffs = np.polyfit(xLog,yLog,1)
    polyFunction = np.poly1d(coeffs)
    xs = np.linspace(0,8,num=100000)
    ys = polyFunction(xs)
    ax.plot(10**xs,10**ys)

    r2 = mseScore(yVals, 10**polyFunction(yLog))


    '''
    xLog = np.log10(xVals)
    yLog = np.log10(yVals)
    coeffs = np.polyfit(xLog,yLog,2)
    print coeffs
    polyFunction = np.poly1d(coeffs)
    xs = np.linspace(2,8,num=100000)
    ys = polyFunction(xs)
    ax.plot(10**xs,10**ys)
    '''

    ax.scatter(xVals,yVals,s=2,color='grey',alpha=0.45, zorder=0)
    #ax.scatter(xVals,scatterYVals2,s=2,color='grey',alpha=0.45, zorder=0)

    ax.text(
        1*10**6.5,
        1*10**0.5,
        "R$^2$: %0.4f" % r2,
        bbox=dict(facecolor='lightgrey', alpha=0.5),
        fontsize=20,
        color='black',
    )
    ax.scatter(binMids,binMeans,s=20,color='black',alpha=1, zorder=0)

    plt.show()

    print "Finished in %0.4f seconds" % (time.time() - startTime)
    return

if __name__ == '__main__':
    #sys.argv = ["programName.py","--input1","data/output/population/2008_population_data.csv","--input2","data/output/groundTruthOutput/2008_groundTruth_data.csv","--input3","data/output/groundTruthOutput/2008_groundTruth_data.csv"]
    #sys.argv = ["programName.py","--input1","data/output/population/2008_population_data.csv","--input3","data/output/radiationOutput/2007_c_0.73_output_data.csv","--input2","data/output/groundTruthOutput/2007_groundTruth_data.csv"]
    #sys.argv = ["programName.py","--input1","data/output/population/2008_population_data.csv","--input2","data/output/groundTruthOutput/2009_groundTruth_data.csv","--input3","data/output/groundTruthOutput/2008_groundTruth_data.csv"]

    main()


import fiona
import rasterio
from rasterio.tools.mask import mask
import numpy as np
import pandas as pd
import progressbar

import matplotlib.pyplot as plt
import matplotlib.cm as cm

from collections import deque

from Data import *

def floodFill(arr,newMask,p1,maxHeight):
    h,w = arr.shape
    maxVal = np.max(arr)

    q = deque([p1]) #this keeps the in memory data small
    qSize = 1 #keep track of size of queue so we don't have to call len(), just in case...

    while qSize>0:
        y,x = q.popleft()
        qSize-=1

        #yuck, sorry
        if 0 <= y < h:
            if 0 <= x-1 < w and arr[y,x-1] <= maxHeight:
                qSize+=1 # increase size of queue
                q.append((y,x-1)) #add neighbor to queue
                newMask[y,x-1]=1 #mark this pixel as reachable by the current water level
                arr[y,x-1] = maxVal+1 #set the original array value to "seen"
            if 0 <= x+1 < w and arr[y,x+1] <= maxHeight:
                qSize+=1
                q.append((y,x+1))
                newMask[y,x+1]=1
                arr[y,x+1] = maxVal+1
        if 0 <= x < w:
            if 0 <= y-1 < h and arr[y-1,x] <= maxHeight:
                qSize+=1
                q.append((y-1,x))
                newMask[y-1,x]=1
                arr[y-1,x] = maxVal+1
            if 0 <= y+1 < h and arr[y+1,x] <= maxHeight:
                qSize+=1
                q.append((y+1,x))
                newMask[y+1,x]=1
                arr[y+1,x] = maxVal+1

    return newMask




origin1 = (2000,7000)
origin2 = (2000,0)

for maxHeight in range(0,11):
    print maxHeight
    src = rasterio.open("usRaster.tif")
    data = src.read()
    src.close()

    data = data.squeeze()

    newMask = np.zeros(data.shape)
    print "First floodfill point is %s with data value %d" % (origin1, data[origin1])
    newMask = floodFill(data,newMask,origin1,maxHeight)
    print "Second floodfill point is %s with data value %d" % (origin2, data[origin2])
    newMask = floodFill(data,newMask,origin2,maxHeight)
    np.save("outputs/mask_%d.npy" % (maxHeight), newMask)

    plt.figure(figsize=(16, 8))
    plt.imshow(newMask, interpolation='bilinear', cmap=cm.gray, alpha=1.0)
    plt.grid(False)
    plt.savefig("outputs/mask_%d.png" % (maxHeight), dpi=80)
    plt.close()

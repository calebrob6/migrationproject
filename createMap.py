import numpy as np

import struct
import cv2
import os

from collections import defaultdict

'''
gridPoints = defaultdict(dict)
baseDir = "data/elevation"
for fn in os.listdir(baseDir):
    lonKey = fn[0]
    lon = int(fn[1:4])
    latKey = fn[4]
    lat = int(fn[5:7])

    if lonKey=='W':
        lon*=-1
    if latKey=='S':
        lat*=-1

    gridPoints[lon][lat]=fn


lonRange = list(enumerate(range(-180,141,40)))
latRange = list(enumerate(range(90,-11,-50)))


width = 4800
height = 6000
scaleFactor = 1

bigWidth = (width/scaleFactor) * 9
bigHeight = (height/scaleFactor) * 3
bigImage = np.zeros((bigHeight,bigWidth), dtype=np.int16)

for x,lon in lonRange:
    for y,lat in latRange:
        print x,y
        fn = gridPoints[lon][lat]
        img = np.load("x%d_y%d_scale%d.npy" % (x,y,scaleFactor))

        tWidth = (width/scaleFactor)
        tHeight = (height/scaleFactor)

        bigImage[y*tHeight:(y+1)*tHeight, x*tWidth:(x+1)*tWidth] = img


np.save("bigImage",bigImage)
'''

img = np.load("bigImage.npy")
img = img[::10,::10]

import scipy.misc
scipy.misc.imsave('outfile.jpg', img)

#cv2.imwrite("test.png",bigImage)

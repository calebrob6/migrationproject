#!/bin/bash

for i in `seq 2000 2015`;
do
  echo "Output population for $i"
  python formatPopulationData.py --input $i --output data/output/population/${i}_population_data.csv
done

for i in `seq 2004 2013`;
do
  echo "Output IRS Ground Truth Table for $i"
  python formatIRSData.py --input $i --output data/output/groundTruthOutput/${i}_groundTruth_data.csv
done

#!/usr/bin/env python
import sys,os
import time
import argparse

import numpy as np
from scipy import stats

import matplotlib.pyplot as plt

def doArgs(argList, name):
    parser = argparse.ArgumentParser(description=name)

    parser.add_argument('-v', "--verbose", action="store_true", help="Enable verbose debugging", default=False)
    parser.add_argument('--population', action="store", dest="populationFn", type=str, help="Population table file name", required=True)
    parser.add_argument('--input1', action="store", dest="inputFn1", type=str, help="Input Ground Truth file name", required=True)
    parser.add_argument('--input2', action="store", dest="inputFn2", type=str, help="Input Radiation Output file name", required=True)

    return parser.parse_args(argList)

def main():
    progName = "Gen Paper Figures"
    args = doArgs(sys.argv[1:], progName)

    verbose = args.verbose
    inputFn1 = args.inputFn1
    inputFn2 = args.inputFn2
    populationFn = args.populationFn

    print "Starting %s" % (progName)
    startTime = float(time.time())

    if not os.path.isfile(inputFn1) or not os.path.isfile(inputFn2) or not os.path.isfile(populationFn):
        print "Input doesn't exist, exiting"
        return

    print "Loading input files"

    f = open(populationFn,"r")
    f.readline()
    lines = f.read().strip().split("\n")
    f.close()
    data = {}
    totalPopulation = 0
    for line in lines:
        line = line.strip()
        if line!="":
            parts = line.split(",")
            countyPopulation = int(parts[3])
            totalPopulation += countyPopulation
            data[int(parts[0])] = countyPopulation


    n = len(data)
    keys = sorted(data.keys()) #we use this to iterate over the dictionary to make sure we go through it the same way each time.
    data2 = {}
    for i,key in enumerate(keys):
        data2[i] = data[key]

    xValTuples = sorted(data2.items(), key=lambda x:x[1])


    data1 = np.loadtxt(inputFn1, dtype=int, delimiter=",", skiprows=1)
    data2 = np.loadtxt(inputFn2, dtype=int, delimiter=",", skiprows=1)
    for i in xrange(data1.shape[0]):
        data1[i,i] = 0
        data2[i,i] = 0

    totalTrips1 = np.sum(data1)
    totalTrips2 = np.sum(data2)
    xVals = []
    pts1 = []
    pts2 = []
    for i,pop in xValTuples:

        yVal1 = np.sum(data1[:,i])
        yVal2 = np.sum(data2[:,i])

        xVals.append(pop)
        pts1.append((pop, yVal1))
        pts2.append((pop, yVal2))


    pts1 = sorted(pts1,key=lambda x:x[0])
    pts2 = sorted(pts2,key=lambda x:x[0])

    NUMBINS = 30
    MIN, MAX = np.min(xVals),np.max(xVals)
    #hist, bin_edges = np.histogram(xVals, bins = 10 ** np.linspace(np.log10(MIN), np.log10(MAX), NUMBINS))
    hist, bin_edges = np.histogram(xVals, bins = np.logspace(np.log10(MIN), np.log10(MAX), NUMBINS))
    print hist

    binMids = np.array([0 for i in range(hist.shape[0])])
    bins1 = [[] for i in range(hist.shape[0])]
    bins2 = [[] for i in range(hist.shape[0])]
    for i in range(0,bin_edges.shape[0]-1):
        lowVal = bin_edges[i]
        highVal = bin_edges[i+1]

        for j in xrange(len(pts1)):
            xVal = pts1[j][0]

            yVal1 = pts1[j][1]
            yVal2 = pts2[j][1]
            if lowVal<=xVal<highVal:
                bins1[i].append(yVal1)
                bins2[i].append(yVal2)

        binMids[i] = (highVal+lowVal)//2


    yVals1 = [np.sum(b)/float(totalTrips1) for b in bins1]
    yVals2 = [np.sum(b)/float(totalTrips2) for b in bins2]

    print sum(yVals1)
    print sum(yVals2)

    fig = plt.figure(figsize=(10,10))
    ax = plt.subplot(111)

    ax.plot(binMids,yVals1,label="Ground Truth", color='black', linestyle='-', marker='o', markerfacecolor='black', markersize=8, linewidth=3)
    ax.plot(binMids,yVals2,label="Radiation Model", color='green', linestyle='--', marker='^', markerfacecolor='green', markersize=8, linewidth=3)

    ax.set_yscale("log")
    ax.set_xscale("log")
    ax.axis([1e1, 1e8, 1e-10,1e-4])

    #box = ax.get_position()
    #ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    lgd = ax.legend(loc='upper right', fontsize=16)

    ax.grid(True)
    ax.set_xlabel('Population, n', fontsize=20)
    ax.set_ylabel('Pdest(n)', fontsize=20)
    ax.set_title("Probability of migration vs. population", fontsize=24)


    plt.show()



    print "Finished in %0.4f seconds" % (time.time() - startTime)
    return

if __name__ == '__main__':
    sys.argv = ["programName.py","--population", "data/output/population/2007_population_data.csv","--input1","data/output/groundTruthOutput/2007_groundTruth_data.csv","--input2","data/output/radiationOutput/2007_c_0.03_output_data.csv"]
    main()

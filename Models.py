import numpy as np
import time


def radiationModel(populationData, distanceMatrix, countyLandCoverPercentages, fitFunction, countyIds, verbose=False):
    startTime = float(time.time())

    size = len(countyIds)
    odMatrix = np.zeros((size, size))

    if verbose:
        print "Running radiation model"
    n = len(countyIds)
    for i, k in enumerate(countyIds):
        if i % 100 == 0 and verbose:
            print "%d/%d\t%0.4f" % (i, n, time.time() - startTime)

        currentPopulation = float(populationData[i])
        #norm = totalPopulation/(totalPopulation-currentPopulation)

        # sort other counties by distance (closest to farthest away)
        otherPatches = []
        for j, k2 in enumerate(countyIds):
            otherPatches.append((distanceMatrix[i][j], j))
        otherPatches.sort(key=lambda x: x[0])
        
        assert otherPatches[0][1] == i
        
        otherPatches = otherPatches[1:]
        
        S = 0
        for distance, p in otherPatches:
            otherPopulation = (1 - countyLandCoverPercentages[p]) * float(populationData[p])

            g = (countyLandCoverPercentages[i] * currentPopulation) + \
                fitFunction((1 - countyLandCoverPercentages[i]) * currentPopulation)

            #num = norm*currentPopulation*cRate*currentPopulation*otherPopulation
            num = g * currentPopulation * otherPopulation
            denom = (currentPopulation + S) * \
                (currentPopulation + S + otherPopulation)
            total = num / denom

            odMatrix[i][p] = int(total)
            S += otherPopulation
    if verbose:
        print "Finished running radiation model in %0.4f seconds" % (time.time() - startTime)
    return odMatrix

def radiationModelImproved(populationData, distanceMatrix, countyLandCoverPercentages, alpha, beta, countyIds, verbose=False):
    startTime = float(time.time())

    size = len(countyIds)
    odMatrix = np.zeros((size, size))

    if verbose:
        print "Running radiation model"
        
    n = len(countyIds)
    for i, k in enumerate(countyIds):
        if i % 100 == 0 and verbose:
            print "%d/%d\t%0.4f" % (i, n, time.time() - startTime)

        currentPopulation = float(populationData[i])
        Ai = float(countyLandCoverPercentages[i])

        p = ((alpha+beta)*(1-Ai)*currentPopulation)+(Ai*currentPopulation)
        
        #---------------------------------------------------
        #Handle self migrants
        numberInteriorMigrants = (1-Ai) * ((beta)/(alpha+beta)) * p
        odMatrix[i][i] = numberInteriorMigrants
        
        #---------------------------------------------------
        #Handle exterior migrations
        
        #Step 1.) Sort counties by distance (closest to farthest away)
        otherPatches = []
        for j, k2 in enumerate(countyIds):
            otherPatches.append((distanceMatrix[i][j], j))
        otherPatches.sort(key=lambda x: x[0])   
        otherPatches = otherPatches[1:] #don't include self

        numberExteriorMigrants =  ((alpha)/(alpha+beta)) * p + ((beta)/(alpha+beta)) * Ai * p
        
        #Step 2.) Incrementally calculate the migrants to other counties
        S = 0
        for distance, j in otherPatches:
            otherPopulation = (1 - countyLandCoverPercentages[j]) * float(populationData[j])
            
            numerator = currentPopulation * otherPopulation
            denominator = (currentPopulation + S) * (currentPopulation + otherPopulation + S)
            
            total = numberExteriorMigrants * (numerator / denominator)

            odMatrix[i][j] = int(total)
            S += otherPopulation
    if verbose:
        print "Finished running radiation model in %0.4f seconds" % (time.time() - startTime)
    return odMatrix


def radiationModelProbabilitiesWrapper(populationData, distanceMatrix, countyLandCoverPercentages, alpha, beta, countyIds):

    
    outgoingMigrants,odMatrix = radiationModelProbabilities(populationData, distanceMatrix, countyLandCoverPercentages, alpha, beta, countyIds)
    
    
    originalProbabilities = odMatrix.copy()
    rowSums = np.sum(originalProbabilities,axis=1)
    
    newProbabilities = (originalProbabilities.T/rowSums).T
    #newRowSums = np.sum(newProbabilities,axis=1)

    T_production_constrained = (newProbabilities.T * outgoingMigrants).T
    #T_production_constrained_rowSums = np.sum(T_production_constrained,axis=1)

    return T_production_constrained

def radiationModelProbabilities(populationData, distanceMatrix, countyLandCoverPercentages, alpha, beta, countyIds):

    size = len(countyIds)
    outgoingMigrants = []
    odMatrix = np.zeros((size, size))
      
    n = len(countyIds)
    for i, k in enumerate(countyIds):
        currentPopulation = float(populationData[i])
        Ai = float(countyLandCoverPercentages[i])

        p = ((alpha+beta)*(1-Ai)*currentPopulation)+(Ai*currentPopulation)
        
        odMatrix[i][i] = 0
        
        #---------------------------------------------------
        #Handle exterior migrations
        
        #Step 1.) Sort counties by distance (closest to farthest away)
        otherPatches = []
        for j, k2 in enumerate(countyIds):
            otherPatches.append((distanceMatrix[i][j], j))
        otherPatches.sort(key=lambda x: x[0])   
        otherPatches = otherPatches[1:] #don't include self

        numberExteriorMigrants =  ((alpha)/(alpha+beta)) * p + ((beta)/(alpha+beta)) * Ai * p
        outgoingMigrants.append(numberExteriorMigrants)
        
        #Step 2.) Incrementally calculate the migrants to other counties
        S = 0
        for distance, j in otherPatches:
            otherPopulation = (1 - countyLandCoverPercentages[j]) * float(populationData[j])
            
            numerator = currentPopulation * otherPopulation
            denominator = (currentPopulation + S) * (currentPopulation + otherPopulation + S)
            
            #total = numberExteriorMigrants * (numerator / denominator)
            total = (numerator / denominator)

            odMatrix[i][j] = float(total)
            S += otherPopulation
    outgoingMigrants = np.array(outgoingMigrants)
    assert outgoingMigrants.shape[0] == odMatrix.shape[0]
    return outgoingMigrants,odMatrix


def radiationModelImprovedDebug(populationData, distanceMatrix, countyLandCoverPercentages, alpha, beta, countyIds, debugIds=[], verbose=False):
    startTime = float(time.time())

    size = len(countyIds)
    odMatrix = np.zeros((size, size))

    if verbose:
        print "Running radiation model"
        
    n = len(countyIds)
    for i, k in enumerate(countyIds):
        if i % 100 == 0 and verbose:
            print "%d/%d\t%0.4f" % (i, n, time.time() - startTime)

        debugMode = False
        if i in debugIds:
            debugMode = True
            
        currentPopulation = float(populationData[i])
        Ai = float(countyLandCoverPercentages[i])

        p = ((alpha+beta)*(1-Ai)*currentPopulation)+(Ai*currentPopulation)
        
        if debugMode:
            print "Debug mode for county %d" % (k)
            print "---------------------------------------"
            print "Current Population (m_i) %d" % (currentPopulation)
            print "Flooded percent (A_i): %0.4f" % (Ai)
            print "Migrants from (unaffected,affected) area: (%0.4f, %0.4f)" % ( ((alpha+beta)*(1-Ai)*currentPopulation), (Ai*currentPopulation))
            print "Total migrants: %0.4f" % (p)
            print "---------------------------------------"
            
        #---------------------------------------------------
        #Handle self migrants
        numberInteriorMigrants = (1-Ai) * ((beta)/(alpha+beta)) * p
        odMatrix[i][i] = numberInteriorMigrants
        
        if debugMode:
            print "Total interior migrants: %d" % (numberInteriorMigrants)
            print "---------------------------------------"
            
        #---------------------------------------------------
        #Handle exterior migrations
        
        #Step 1.) Sort counties by distance (closest to farthest away)
        otherPatches = []
        for j, k2 in enumerate(countyIds):
            otherPatches.append((distanceMatrix[i][j], j))
        otherPatches.sort(key=lambda x: x[0])   
        otherPatches = otherPatches[1:] #don't include self
        
        numberExteriorMigrants =  ((alpha)/(alpha+beta)) * p + ((beta)/(alpha+beta)) * Ai * p
        
        if debugMode:
            print "Exterior migrants from external migration: %0.4f" % (((alpha)/(alpha+beta)) * p)
            print "Exterior migrants from lack of space: %0.4f" % (((beta)/(alpha+beta)) * Ai * p)
            print "Total exterior migrants: %d" % (numberExteriorMigrants)
            print "---------------------------------------"
        
        
        #Step 2.) Incrementally calculate the migrants to other counties
        S = 0
        for distance, j in otherPatches:
            otherPopulation = (1 - countyLandCoverPercentages[j]) * float(populationData[j])
            
            numerator = currentPopulation * otherPopulation
            denominator = (currentPopulation + S) * (currentPopulation + otherPopulation + S)
            
            total = numberExteriorMigrants * (numerator / denominator)

            odMatrix[i][j] = int(total)
            S += otherPopulation
    if verbose:
        print "Finished running radiation model in %0.4f seconds" % (time.time() - startTime)
    return odMatrix


def gravityModel(populationData, distanceMatrix, countyLandCoverPercentages, fitFunction, countyIds):
    startTime = float(time.time())

    size = len(countyIds)
    odMatrix = np.zeros((size, size))

    print "Finished running radiation model in %0.4f seconds" % (time.time() - startTime)
    return odMatrix

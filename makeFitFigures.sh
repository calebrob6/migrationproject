#!/bin/bash


for i in `seq 2004 2012`;
do
  echo "Fit figure for $i"
  python generateFitFigure.py --inputPop data/output/population/${i}_population_data.csv --inputMatrix data/output/groundTruthOutput/${i}_groundTruth_data.csv --year ${i}
done


#python generateFitFigure.py --inputPop data/output/population/2007_population_data.csv --inputMatrix data/output/groundTruthOutput/2007_groundTruth_data.csv --year 2007
#python generateFitFigure.py --inputPop data/output/population/2008_population_data.csv --inputMatrix data/output/groundTruthOutput/2008_groundTruth_data.csv --year 2008
#python generateFitFigure.py --inputPop data/output/population/2009_population_data.csv --inputMatrix data/output/groundTruthOutput/2009_groundTruth_data.csv --year 2009
#python generateFitFigure.py --inputPop data/output/population/2010_population_data.csv --inputMatrix data/output/groundTruthOutput/2010_groundTruth_data.csv --year 2010

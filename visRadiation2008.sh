#!/bin/bash


python convertODMatrix.py --input data/output/groundTruthOutput/2008_groundTruth_data.csv --output data/output/visData/2008/groundTruth/
python convertODMatrix.py --input data/output/radiationOutput/2008_c_0.10_output_data.csv --output data/output/visData/2008/radiationOutput/
python convertODMatrix.py --input data/output/radiationOutput/2008_c_0.90_output_data.csv --output data/output/visData/2008/radiationOutput2/

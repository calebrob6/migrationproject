import fiona
import shapely.geometry
import matplotlib
import matplotlib.collections
import matplotlib.colors
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import LinearSegmentedColormap

import pandas as pd

import descartes
import itertools

import numpy as np

from mpl_toolkits.basemap import Basemap

import pysal.esda.mapclassify


def colorbar_index(ncolors, cmap, labels=None, **kwargs):
    """
    This is a convenience function to stop you making off-by-one errors
    Takes a standard colour ramp, and discretizes it,
    then draws a colour bar with correctly aligned labels
    """
    cmap = cmap_discretize(cmap, ncolors)
    mappable = cm.ScalarMappable(cmap=cmap)
    mappable.set_array([])
    mappable.set_clim(-0.5, ncolors+0.5)
    colorbar = plt.colorbar(mappable, **kwargs)
    colorbar.set_ticks(np.linspace(0, ncolors, ncolors))
    colorbar.set_ticklabels(range(ncolors))
    if labels:
        colorbar.set_ticklabels(labels)
    return colorbar

def cmap_discretize(cmap, N):
    """
    Return a discrete colormap from the continuous colormap cmap.

        cmap: colormap instance, eg. cm.jet.
        N: number of colors.

    Example
        x = resize(arange(100), (5,100))
        djet = cmap_discretize(cm.jet, 5)
        imshow(x, cmap=djet)

    """
    if type(cmap) == str:
        cmap = get_cmap(cmap)
    colors_i = np.concatenate((np.linspace(0, 1., N), (0., 0., 0., 0.)))
    colors_rgba = cmap(colors_i)
    indices = np.linspace(0, 1., N + 1)
    cdict = {}
    for ki, key in enumerate(('red', 'green', 'blue')):
        cdict[key] = [(indices[i], colors_rgba[i - 1, ki], colors_rgba[i, ki]) for i in xrange(N + 1)]
    return matplotlib.colors.LinearSegmentedColormap(cmap.name + "_%d" % N, cdict, 1024)



class Mapping(object):

    def __init__(self, lats, lons, shapefileName, interestingIds, colorBarLocation=[0.85,0.12,0.01,0.47], showScale=True, tickSize=26):
        self.lats = lats
        self.lons = lons

        if shapefileName.endswith(".shp"):
            shapefileName = shapefileName[:-4]
        self.shapefileName = shapefileName

        '''
        these are the primary keys from the shapefile that we are interested in,
        anything not in this list will be ignored
        '''
        self.interestingIds = interestingIds

        #make white --> red colormap
        '''
        cdictCustom = {
            'red':   ((0.0, 1.0, 1.0),
                      (1.0, 1.0, 1.0)),

            'green': ((0.0, 1.0, 1.0),
                      (1.0, 0.0, 0.0)),

            'blue': ((0.0, 1.0, 1.0),
                      (1.0, 0.0, 0.0)),
        }
        '''

        singleBreakPoint = 0.5 #if this is 0.5 color bar will be linear
        cdictCustom = {
            'red':   ((0.0,  1.0, 1.0),
                      (singleBreakPoint, 1.0, 1.0),
                      (1.0,  1.0, 1.0)),

            'green': ((0.0,  1.0, 1.0),
                      (singleBreakPoint, 0.5, 0.5),
                      (1.0,  0.0, 0.0)),

            'blue': ((0.0,  1.0, 1.0),
                     (singleBreakPoint, 0.5, 0.5),
                     (1.0,  0.0, 0.0)),
        }
        white_red = matplotlib.colors.LinearSegmentedColormap('WhiteRed', cdictCustom)


        #make blue --> white --> red colormap where the white --> blue and white --> red transitions happen less quickly
        doubleBreakPoint = 0.25
        cdictCustom = {
            'red':   ((0.0,  0.0, 0.0),
                      (doubleBreakPoint, 0.5, 0.5),
                      (0.5,  1.0, 1.0),
                      (1-doubleBreakPoint, 1.0, 1.0),
                      (1.0,  1.0, 1.0)),

            'green': ((0.0,  0.0, 0.0),
                      (doubleBreakPoint, 0.5, 0.5),
                      (0.5,  1.0, 1.0),
                      (1-doubleBreakPoint, 0.5, 0.5),
                      (1.0,  0.0, 0.0)),

            'blue':  ((0.0,  1.0, 1.0),
                      (doubleBreakPoint, 1.0, 1.0),
                      (0.5,  1.0, 1.0),
                      (1-doubleBreakPoint, 0.5, 0.5),
                      (1.0,  0.0, 0.0)),
        }
        blue_white_red = matplotlib.colors.LinearSegmentedColormap('BlueWhiteRed', cdictCustom)



        self.cmap = plt.get_cmap('Reds')
        self.cmap = white_red

        self.difference_cmap = plt.get_cmap('seismic')
        self.difference_cmap = blue_white_red


        self.colorBarLocation = colorBarLocation
        self.showScale = showScale

        self.tickSize = tickSize

    #check to see if a lat, lon pair is within the bounding box that we are interested in
    def isIn(self, lat, lon):
        return self.lats[0] <= lat <= self.lats[1] and self.lons[0] <= lon <= self.lons[1]


    def anyAreIn(self, lonList, latList):
        assert len(lonList) == len(latList), "Lattitude and longitude lists are different sizes, something is wrong"
        for i in range(len(latList)):
            lat,lon = latList[i], lonList[i]
            if self.isIn(lat,lon):
                return True
        return False

    def plotSingleMap(self, title, outputFn, data, colorBarRange, colorMap=None, highlightId=None, showFig=True, useLogScale=True, embeddedColorBar=3):

        colorBarMin = colorBarRange[0]
        colorBarMax = colorBarRange[1]

        tempCmap = self.cmap
        if colorMap!=None:
            self.cmap = colorMap

        fig = plt.figure()
        ax = fig.add_subplot(111, axisbg='#ffffff', frame_on=False)

        m = Basemap(
            projection='merc',
            llcrnrlat=self.lats[0],
            urcrnrlat=self.lats[1],
            llcrnrlon=self.lons[0],
            urcrnrlon=self.lons[1],
            resolution=None,
            fix_aspect=True,
            suppress_ticks=True
        )

        polyList = []
        fipsList = []
        dataList = []
        highlightList = []

        m.readshapefile(
            self.shapefileName,
            "shapes",
            drawbounds = False
        )

        for geometry, info in zip(m.shapes, m.shapes_info):
            lats = []
            lons = []
            for x,y in geometry:
                lon,lat = m(x,y,inverse=True)
                lats.append(lat)
                lons.append(lon)

            idVal = int(info["STATEFP"]+info["COUNTYFP"])
            if self.anyAreIn(lons,lats) and idVal in self.interestingIds:
                fipsList.append(idVal)
                polyList.append(shapely.geometry.Polygon(geometry))
                dataList.append(data[idVal])
                if highlightId==None:
                    highlightList.append(False)
                else:
                    if idVal == highlightId:
                        highlightList.append(True)
                    else:
                        highlightList.append(False)

        '''
        The following code tries to load all of the shapes/info using fiona, it currently doesn't handle MultiPolygons
        correctly. Basemap handles MultiPolygons by making a copy of all the data for each Polygon (e.g. if county 1 is made
        up of 19 Polygons, Basemap will create 19 indivdual entries with all the data from county 1 for each entry)
        '''

        '''
        shp = fiona.open(self.shapefileName+".shp")
        for v in shp.values():
            idVal = int(v["properties"]["STATEFP"]+v["properties"]["COUNTYFP"])
            #we can add a check here to see if the shape is partially within our self.lats, self.lons
            if idVal in self.interestingIds:
                if v["geometry"]["type"]=="MultiPolygon":
                    joinedPolygonList = [_[0] for _ in v["geometry"]["coordinates"]]
                    tempPolys = [[m(_[0],_[1]) for _ in polygon] for polygon in joinedPolygonList]
                    for poly in tempPolys:
                        polyList.append(shapely.geometry.Polygon(poly))
                        fipsList.append(idVal)
                        dataList.append(data[idVal])
                else:
                    coordList = [m(_[0],_[1]) for _ in v["geometry"]["coordinates"][0]]
                    polyList.append(shapely.geometry.Polygon(coordList))
                    fipsList.append(idVal)
                    dataList.append(data[idVal])
        shp.close()
        '''

        df_map = pd.DataFrame({
            'poly': polyList,
            'cnt_fip': fipsList,
            'dataPoint': dataList,
            'highlight': highlightList
        })



        df_map['patches'] = df_map['poly'].map(lambda x: descartes.PolygonPatch(x, ec='#555555', lw=.2, alpha=1.0, zorder=4))


        patchCollection = matplotlib.collections.PatchCollection(df_map['patches'], match_original=True)

        if not useLogScale:
            norm = matplotlib.colors.Normalize(vmin=colorBarMin, vmax=colorBarMax)
        else:
            norm = matplotlib.colors.LogNorm(vmin=colorBarMin, vmax=colorBarMax)
        faceColorValues = self.cmap(norm(df_map['dataPoint'].values))

        patchCollection.set_facecolor(faceColorValues)
        ax.add_collection(patchCollection)

        scalarMap = matplotlib.cm.ScalarMappable(cmap=self.cmap, norm=norm)
        scalarMap.set_clim(colorBarMin, colorBarMax)
        scalarMap.set_array(df_map['dataPoint'].values)


        if highlightId!=None:
            currentEdgeColors = patchCollection.get_edgecolors().tolist()
            currentLineWidths = patchCollection.get_linewidths()
            for i,highlightIt in enumerate(df_map['highlight'] == True):
                if highlightIt:
                    currentEdgeColors[i] = 'k'
                    currentLineWidths[i] = 3
            patchCollection.set_edgecolors(currentEdgeColors)
            patchCollection.set_linewidths(currentLineWidths)

        #will overwrite any c
        if self.showScale:
            m.drawmapscale(
                self.lons[0]+6, self.lats[0]+1.5, self.lons[0], self.lats[0],
                1000,
                barstyle='fancy',
                labelstyle='simple',
                fillcolor1='#ffffff',
                fillcolor2='#555555',
                fontcolor='#555555',
                zorder=5,
                ax=ax
            )

        m.drawmapboundary(
            color='k',
            linewidth=0.0,
            fill_color='#ffffff',
            zorder=None,
            ax=ax
        )

        ax.set_title(
            title,
            fontsize=26
        )

        maxlog=int(np.ceil( np.log10(colorBarMax) ))
        tick_locations_plot=([(10.0**x) for x in range(0,maxlog+1)])
        tick_labels = (["$10^%d$"%x for x in range(0,maxlog+1)])
        if embeddedColorBar==2:
            #left, bottom, width, height
            cbaxes = fig.add_axes(self.colorBarLocation)
            colorBar = fig.colorbar(
                scalarMap,
                orientation='vertical',
                ticks=tick_locations_plot,
                cax=cbaxes,
            )
        elif embeddedColorBar==1:
            colorBar = plt.colorbar(
                scalarMap,
                shrink=0.9,
                orientation='vertical',
                pad=0.05,
                ticks=tick_locations_plot
            )
        elif embeddedColorBar==3:
            #left, bottom, width, height
            cbaxes = fig.add_axes(self.colorBarLocation)
            colorBar = fig.colorbar(
                scalarMap,
                orientation='horizontal',
                ticks=tick_locations_plot,
                cax=cbaxes,
            )

        colorBar.ax.set_xticklabels(tick_labels)
        colorBar.ax.set_yticklabels(tick_labels)
        colorBar.ax.tick_params(labelsize=self.tickSize,color='k',labelcolor='k')

        fig.set_size_inches(20, 10)
        plt.savefig(outputFn, alpha=True, bbox_inches='tight')
        if showFig:
            plt.show()

        self.cmap = tempCmap


    def getUserBins(self,data,numBins):
        dataMax = max(data.values())
        stepVal = dataMax/numBins
        binVals = np.arange(0,dataMax+stepVal,stepVal).tolist()
        return binVals


    def plotBinnedMap(self, title, outputFn, data, useNaturalBreaks=True, numNaturalBreaks=5, userBins=None, highlightId=None, showFig=True):

        if useNaturalBreaks and userBins!=None:
            raise Exception("Can't use natural breaks and user defined breaks.")

        #------------------------------------------------------------------------------------------------------------------------------------
        #Setup figure

        fig = plt.figure()
        ax = fig.add_subplot(111, axisbg='w', frame_on=False)

        #------------------------------------------------------------------------------------------------------------------------------------
        #Load map

        m = Basemap(
            projection='merc',
            llcrnrlat=self.lats[0],
            urcrnrlat=self.lats[1],
            llcrnrlon=self.lons[0],
            urcrnrlon=self.lons[1],
            resolution=None,
            fix_aspect=False,
            suppress_ticks=True
        )

        #------------------------------------------------------------------------------------------------------------------------------------
        #Load shapefile and link assosciated data

        polyList = []
        fipsList = []
        dataList = []
        highlightList = []

        m.readshapefile(
            self.shapefileName,
            "shapes",
            drawbounds = False
        )

        for geometry, info in zip(m.shapes, m.shapes_info):
            lats = []
            lons = []
            for x,y in geometry:
                lon,lat = m(x,y,inverse=True)
                lats.append(lat)
                lons.append(lon)

            idVal = int(info["STATEFP"]+info["COUNTYFP"])
            if self.anyAreIn(lons,lats) and idVal in self.interestingIds:
                fipsList.append(idVal)
                polyList.append(shapely.geometry.Polygon(geometry))
                dataList.append(data[idVal])
                if highlightId==None:
                    highlightList.append(False)
                else:
                    if idVal == highlightId:
                        highlightList.append(True)
                    else:
                        highlightList.append(False)


        df_map = pd.DataFrame({
            'poly': polyList,
            'cnt_fip': fipsList,
            'dataPoint': dataList,
            'highlight': highlightList
        })

        #------------------------------------------------------------------------------------------------------------------------------------
        #Setup breaks for the colorbar, there are a bunch of ways we can do this from pysal.esda.mapclassify

        breaks = None
        if useNaturalBreaks:
            breaks = pysal.esda.mapclassify.Natural_Breaks(df_map.dataPoint.values, k=numNaturalBreaks)
        elif userBins!=None:
            breaks = pysal.esda.mapclassify.User_Defined(df_map.dataPoint.values,userBins)
        else:
            raise Exception("You have not defined a break style.")

        #breaks.yb will have an integer value from [0-numBreaks] that represents which bin each shape is in
        jb = pd.DataFrame({'jenks_bins': breaks.yb}, index=df_map.index)
        df_map = df_map.join(jb)

        #this sets up the bin labels with counts of how many items are in each bin
        binLabels = ["< %0.1f (%d items)" % (b,c) for b, c in zip(breaks.bins, breaks.counts)]

        df_map['patches'] = df_map['poly'].map(lambda x: descartes.PolygonPatch(x, ec='#555555', lw=.2, alpha=1., zorder=4))
        pc = matplotlib.collections.PatchCollection(df_map['patches'], match_original=True)

        #this sets up a normalizer whose max value is the largest bin number (number of bins - 1)
        jenksMin = 0
        jenksMax = df_map['jenks_bins'].max()
        norm = matplotlib.colors.Normalize(vmin=jenksMin, vmax=jenksMax)

        #we split the colorspace evenly between the number of bin values
        faceColorValues = self.cmap(norm(df_map['jenks_bins'].values))
        pc.set_facecolor(faceColorValues)
        ax.add_collection(pc)

        #adding the colorbar
        cb = colorbar_index(ncolors=len(binLabels), cmap=self.cmap, shrink=.8, pad=0.03, labels=binLabels)
        cb.ax.tick_params(labelsize=20)

        if highlightId!=None:
            currentEdgeColors = patchCollection.get_edgecolors().tolist()
            currentLineWidths = patchCollection.get_linewidths()
            for i,highlightIt in enumerate(df_map['highlight'] == True):
                if highlightIt:
                    currentEdgeColors[i] = 'k'
                    currentLineWidths[i] = 3
            patchCollection.set_edgecolors(currentEdgeColors)
            patchCollection.set_linewidths(currentLineWidths)

        m.drawmapscale(
            self.lons[0]+6, self.lats[0]+1.5, self.lons[0], self.lats[0],
            1000,
            barstyle='fancy',
            labelstyle='simple',
            fillcolor1='#ffffff',
            fillcolor2='#555555',
            fontcolor='#555555',
            zorder=5,
            ax=ax
        )

        m.drawmapboundary(
            color='k',
            linewidth=0.0,
            fill_color='#ffffff',
            zorder=None,
            ax=ax
        )

        plt.title(title, fontsize=26)

        fig.set_size_inches(22, 10)
        fig.tight_layout()
        plt.savefig(outputFn, alpha=True)
        if showFig:
            plt.show()

    def plotDifferenceMap(self, title, outputFn, data, colorBarAbs, highlightId=None, showFig=True, embeddedColorBar=3):
        colorBarMin = -colorBarAbs
        colorBarMax = colorBarAbs

        fig = plt.figure()
        ax = fig.add_subplot(111, axisbg='#ffffff', frame_on=False)

        m = Basemap(
            projection='merc',
            llcrnrlat=self.lats[0],
            urcrnrlat=self.lats[1],
            llcrnrlon=self.lons[0],
            urcrnrlon=self.lons[1],
            resolution=None,
            fix_aspect=True,
            suppress_ticks=True
        )

        polyList = []
        fipsList = []
        dataList = []
        highlightList = []

        m.readshapefile(
            self.shapefileName,
            "shapes",
            drawbounds = False
        )

        for geometry, info in zip(m.shapes, m.shapes_info):
            lats = []
            lons = []
            for x,y in geometry:
                lon,lat = m(x,y,inverse=True)
                lats.append(lat)
                lons.append(lon)

            idVal = int(info["STATEFP"]+info["COUNTYFP"])
            if self.anyAreIn(lons,lats) and idVal in self.interestingIds:
                fipsList.append(idVal)
                polyList.append(shapely.geometry.Polygon(geometry))
                dataList.append(data[idVal])
                if highlightId==None:
                    highlightList.append(False)
                else:
                    if idVal == highlightId:
                        highlightList.append(True)
                    else:
                        highlightList.append(False)


        df_map = pd.DataFrame({
            'poly': polyList,
            'cnt_fip': fipsList,
            'dataPoint': dataList,
            'highlight': highlightList
        })

        df_map['patches'] = df_map['poly'].map(lambda x: descartes.PolygonPatch(x, ec='#555555', lw=.2, alpha=1.0, zorder=4))


        patchCollection = matplotlib.collections.PatchCollection(df_map['patches'], match_original=True)

        #norm = matplotlib.colors.Normalize(vmin=colorBarMin, vmax=colorBarMax)
        symlogthreshold = 1
        norm = matplotlib.colors.SymLogNorm(symlogthreshold,vmin=colorBarMin, vmax=colorBarMax)
        faceColorValues = self.difference_cmap(norm(df_map['dataPoint'].values))
        patchCollection.set_facecolor(faceColorValues)
        ax.add_collection(patchCollection)

        scalarMap = matplotlib.cm.ScalarMappable(cmap=self.difference_cmap, norm=norm)
        scalarMap.set_clim(colorBarMin, colorBarMax)
        scalarMap.set_array(df_map['dataPoint'].values)


        maxlog=int(np.ceil( np.log10(colorBarMax) ))
        minlog=int(np.ceil( np.log10(-colorBarMin) ))
        logthresh = int(np.ceil(np.log10(symlogthreshold)))
        tick_locations_plot=( [-(10.0**x) for x in range(minlog-1,-logthresh-1,-1)]
                  #+ [0.0]
                  + [(10.0**x) for x in range(-logthresh,maxlog+1)]
                   )
        tick_labels = (["-$10^%d$"%x for x in range(minlog-1,-logthresh-1,-1)]
                    #+ ["$0$"]
                    + ["$10^%d$"%x for x in range(-logthresh,maxlog+1)])

        if highlightId!=None:
            currentEdgeColors = patchCollection.get_edgecolors().tolist()
            currentLineWidths = patchCollection.get_linewidths()
            for i,highlightIt in enumerate(df_map['highlight'] == True):
                if highlightIt:
                    currentEdgeColors[i] = 'k'
                    currentLineWidths[i] = 3
            patchCollection.set_edgecolors(currentEdgeColors)
            patchCollection.set_linewidths(currentLineWidths)


        if self.showScale:
            m.drawmapscale(
                self.lons[0]+6, self.lats[0]+1.5, self.lons[0], self.lats[0],
                1000,
                barstyle='fancy',
                labelstyle='simple',
                fillcolor1='#ffffff',
                fillcolor2='#555555',
                fontcolor='#555555',
                zorder=5,
                ax=ax
            )

        m.drawmapboundary(
            color='k',
            linewidth=0.0,
            fill_color='#ffffff',
            zorder=None,
            ax=ax
        )

        ax.set_title(
            title,
            fontsize=26
        )

        #put tight layout here if necessary

        if embeddedColorBar==2:
            #left, bottom, width, height
            cbaxes = fig.add_axes(self.colorBarLocation)
            colorBar = fig.colorbar(
                scalarMap,
                orientation='vertical',
                ticks=tick_locations_plot,
                cax=cbaxes,
            )
        elif embeddedColorBar==1:
            colorBar = plt.colorbar(
                scalarMap,
                shrink=0.9,
                orientation='vertical',
                pad=0.05,
                ticks=tick_locations_plot
            )
        elif embeddedColorBar==3:
            #left, bottom, width, height
            cbaxes = fig.add_axes(self.colorBarLocation)
            colorBar = fig.colorbar(
                scalarMap,
                orientation='horizontal',
                ticks=tick_locations_plot,
                cax=cbaxes,
            )

        colorBar.ax.set_xticklabels(tick_labels)
        colorBar.ax.set_yticklabels(tick_labels)
        colorBar.ax.tick_params(labelsize=self.tickSize,color='k',labelcolor='k')

        fig.set_size_inches(20, 10)
        plt.savefig(outputFn, alpha=True, bbox_inches='tight')
        if showFig:
            plt.show()





'''
TODO: make the highlight code work in other places
'''

if __name__ == '__main__':
    from Data import *
    import random

    lats = (23.97, 35.82)
    lons = (-96.50, -76.11)

    lats = (24.396308, 49.384358)
    lons = (-124.848974, -66.885444)

    counties = loadCounties()

    data = {_:10**random.randint(0,6) for _ in counties}
    data1 = {_:10**random.randint(0,6) for _ in counties}
    data2 = {_:10**random.randint(0,6) for _ in counties}

    dataDiff = {_:data1[_]-data2[_] for _ in counties}

    for _ in counties[:100]:
        dataDiff[_] = 0

    testMap = Mapping(lats, lons, "data/countyBoundaries/countyBoundaries2.shp", counties, colorBarLocation=[0.9,0.12,0.01,0.47])

    testMap.plotSingleMap("Test Map", "tmp/tmp.pdf", data, (1,100000), highlightId=12086, showFig=True)


    #userBins = testMap.getUserBins(data,6)
    #testMap.plotBinnedMap("Test Map", "/tmp/tmp.png", data, userBins=userBins, showFig=True)
    #testMap.plotBinnedMap("Test Map", "/tmp/tmp.png", data, showFig=True)


    #colorBarAbs = max(np.abs(dataDiff.values()))
    #print colorBarAbs
    #testMap.plotDifferenceMap("Test Map", "/tmp/tmp.png", dataDiff, colorBarAbs, highlightId=1001, showFig=True)

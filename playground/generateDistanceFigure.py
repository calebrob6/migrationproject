#!/usr/bin/env python
import sys,os
import time
import argparse

import numpy as np
from scipy import stats

import matplotlib.pyplot as plt

def doArgs(argList, name):
    parser = argparse.ArgumentParser(description=name)

    parser.add_argument('-v', "--verbose", action="store_true", help="Enable verbose debugging", default=False)
    parser.add_argument('--input1', action="store", dest="inputFn1", type=str, help="Input Ground Truth file name", required=True)
    parser.add_argument('--input2', action="store", dest="inputFn2", type=str, help="Input Radiation Output file name", required=True)

    return parser.parse_args(argList)

def main():
    progName = "Gen Paper Figures"
    args = doArgs(sys.argv[1:], progName)

    verbose = args.verbose
    inputFn1 = args.inputFn1
    inputFn2 = args.inputFn2

    print "Starting %s" % (progName)
    startTime = float(time.time())

    if not os.path.isfile(inputFn1) or not os.path.isfile(inputFn2):
        print "Input doesn't exist, exiting"
        return

    print "Loading input files"
    data1 = np.loadtxt(inputFn1, dtype=int, delimiter=",", skiprows=1)
    data2 = np.loadtxt(inputFn2, dtype=int, delimiter=",", skiprows=1)

    for i in xrange(data1.shape[0]):
        data1[i,i] = 0
        data2[i,i] = 0


    if not os.path.exists("outputDistanceMatrix.npy"):
        print "Calculating distance matrix"
        f = open("data/input/visualizerOutputFormat.csv","r")
        f.readline()
        lines = f.read().strip().split("\n")
        f.close()

        data = {}
        for line in lines:
            line = line.strip()
            if line!="":
                parts = line.split(",")
                data[int(parts[0])] = parts
        keys = sorted(data.keys())
        coords = [(float(data[k][4]),float(data[k][5])) for k in keys]
        distanceMatrix = scipy.spatial.distance.cdist(coords,coords,haversine)
        #np.savetxt("outputDistanceMatrix.csv", distanceMatrix, fmt='%f', delimiter=',', newline='\n')
        np.save("outputDistanceMatrix.npy",distanceMatrix)
    else:
        print "Loading the distance matrix"
        #distanceMatrix = np.loadtxt("outputDistanceMatrix.csv", dtype=float, delimiter=",")
        distanceMatrix = np.load("outputDistanceMatrix.npy")

    totalPopData1 = float(np.sum(data1.flatten()))

    pts1 = []
    pts2 = []
    for i in xrange(distanceMatrix.shape[0]):
        for j in xrange(i+1,distanceMatrix.shape[1]):
            if data1[i,j]!=0 or data1[j,i]!=0:
                #sum1 = float(np.sum(data1[i,:]) + np.sum(data1[:,i]))
                #sum2 = float(np.sum(data1[j,:]) + np.sum(data1[:,j]))
                sum1 = float(np.sum(data1[i,:]))
                sum2 = float(np.sum(data1[j,:]))
                term1 = data1[i,j] / sum1 if sum1!=0 else 0
                term2 = data1[j,i] / sum2 if sum2!=0 else 0
                pts1.append((distanceMatrix[i,j],term1+term2))

                #sum1 = float(np.sum(data2[i,:]) + np.sum(data2[:,i]))
                #sum2 = float(np.sum(data2[j,:]) + np.sum(data2[:,j]))
                sum1 = float(np.sum(data2[i,:]))
                sum2 = float(np.sum(data2[j,:]))
                term1 = data2[i,j] / sum1 if sum1!=0 else 0
                term2 = data2[j,i] / sum2 if sum2!=0 else 0
                pts2.append((distanceMatrix[i,j],term1+term2))


    pts1 = sorted(pts1,key=lambda x:x[0])
    pts2 = sorted(pts2,key=lambda x:x[0])

    xVals = []
    scatterYVal1 = []
    scatterYVal2 = []
    for i in xrange(len(pts1)):
        xVals.append(pts1[i][0])
        scatterYVal1.append(pts1[i][1])
        scatterYVal2.append(pts2[i][1])

    NUMBINS = 30
    MIN, MAX = np.min(xVals),np.max(xVals)
    hist, bin_edges = np.histogram(xVals, bins = 10 ** np.linspace(np.log10(MIN), np.log10(MAX), NUMBINS))
    print hist

    binMids = np.array([0 for i in range(hist.shape[0])])
    bins1 = [[] for i in range(hist.shape[0])]
    bins2 = [[] for i in range(hist.shape[0])]
    for i in range(0,bin_edges.shape[0]-1):
        lowVal = bin_edges[i]
        highVal = bin_edges[i+1]

        for j in xrange(len(pts1)):
            xVal = pts1[j][0]

            yVal1 = pts1[j][1]
            yVal2 = pts2[j][1]
            if lowVal<=xVal<highVal:
                bins1[i].append(yVal1)
                bins2[i].append(yVal2)

        binMids[i] = (highVal+lowVal)//2


    yVals1 = [np.mean(b) for b in bins1]
    yVals2 = [np.mean(b) for b in bins2]

    print [len(b) for b in bins1]
    print [len(b) for b in bins2]

    fig = plt.figure(figsize=(10,10))
    ax = plt.subplot(111)

    ax.scatter(xVals,scatterYVal1, alpha=0.06, color="black", s=1)
    ax.scatter(xVals,scatterYVal2, alpha=0.06, color="green", s=1)

    ax.plot(binMids,yVals1,label="Ground Truth", color='black', linestyle='-', marker='o', markerfacecolor='black', markersize=8, linewidth=3)
    ax.plot(binMids,yVals2,label="Radiation Model", color='green', linestyle='--', marker='^', markerfacecolor='green', markersize=8, linewidth=3)

    ax.set_yscale("log")
    ax.set_xscale("log")
    ax.axis([1, 10000, 0.00000001, 1])

    #box = ax.get_position()
    #ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    lgd = ax.legend(loc='upper right', fontsize=16)

    ax.grid(True)
    ax.set_xlabel('Distance [km], r', fontsize=20)
    ax.set_ylabel('Pdist(r)', fontsize=20)
    ax.set_title("Probability of migration vs. distance", fontsize=24)


    plt.show()

    print "Finished in %0.4f seconds" % (time.time() - startTime)
    return

if __name__ == '__main__':
    sys.argv = ["programName.py","--input1","data/output/groundTruthOutput/2008_groundTruth_data.csv","--input2","data/output/radiationOutput/2008_c_0.10_output_data.csv"]
    main()

import matplotlib.pyplot as plt
import numpy as np

# fake up some data
spread = [2]
center = [10]
flier_high = [20]
flier_low = [3]
data = np.array([[2],[10],[20],[3]])

# basic plot
plt.boxplot(data)
plt.show()

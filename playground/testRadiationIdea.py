import os,shutil
from Data import *

def resetPath(fn):
    if os.path.exists(fn):
        print "Path %s exists, removing" % fn
        shutil.rmtree(fn)
    os.makedirs(fn)
    print "Creating %s" % fn

def main():
    print "Starting test"

    dataFrame = loadFile(year=2010)

    f = open("data/USA_Counties.csv","r")
    f.readline()
    lines = f.read().strip().split("\n")
    f.close()


    countyIds = []
    for line in lines:
        parts = line.strip().split(",")
        countyId = int(parts[0])
        countyIds.append(countyId)

    numMissed=0
    for countyId in countyIds:
        if countyId not in dataFrame.inflows:
            numMissed+=1
    print "Number of missed ",numMissed

    numMissed=0
    for countyId in dataFrame.inflows.keys():
        if countyId not in countyIds:
            numMissed+=1
    print "Number of missed ",numMissed

    numMissed=0
    for countyId in countyIds:
        if countyId not in dataFrame.outflows:
            numMissed+=1
    print "Number of missed ",numMissed

    numMissed=0
    for countyId in dataFrame.outflows.keys():
        if countyId not in countyIds:
            numMissed+=1
    print "Number of missed ",numMissed



    #inflows
    inflowsFn = "data/county_inflows2010"
    inflowKeys = sorted(dataFrame.inflows.keys())
    resetPath(inflowsFn)
    for origin in inflowKeys:
        destinations = dataFrame.inflows[origin]
        f = open("%s/%d.csv"%(inflowsFn,origin),"w")
        f.write("id,commuters\n")
        for dest in inflowKeys:
            commuters = destinations[dest]
            f.write("%d,%d\n" % (dest,commuters))
        f.close()

    #outflows
    outflowsFn = "data/county_outflows2010"
    outflowKeys = sorted(dataFrame.outflows.keys())
    resetPath(outflowsFn)
    for origin in outflowKeys:
        destinations = dataFrame.outflows[origin]
        f = open("%s/%d.csv"%(outflowsFn,origin),"w")
        f.write("id,commuters\n")
        for dest in outflowKeys:
            commuters = destinations[dest]
            f.write("%d,%d\n" % (dest,commuters))
        f.close()

    print "Finished"

if __name__ == '__main__':
    main()

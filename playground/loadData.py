import sys,os
import time

import argparse

import numpy as np

def main():
    startTime = float(time.time())

    parser = argparse.ArgumentParser(description='Load Migration Data')

    parser.add_argument('-v', "--verbose", action="store_true", help="Enable verbose debugging", default=False)
    parser.add_argument('-i', action="store", dest="inputFn", type=str, help="Input file name", required=True)

    #args = parser.parse_args(sys.argv[1:])
    args = parser.parse_args(["-i","data/2004_2005/countyin0405us1.dat"])

    fn = args.inputFn

    f = open(fn,"r")
    lines = []
    for line in f:
        line = line.strip()
        if line != "":
            parts = [a for a in line.split(" ") if a!="" and a.isdigit()]
            lines.append(parts)
            if len(lines)>1: assert len(parts)==len(lines[-1])
    f.close()
    print "Finished loading %d records" % (len(lines))


    print "Finished in %0.4f seconds" % (time.time() - startTime)
    return

if __name__ == '__main__':
    main()

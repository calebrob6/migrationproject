import geopandas as gp
from shapely.geometry import Point, Polygon, MultiPoint, MultiPolygon

def convert_3D_2D(geometry):
    '''
    Takes a GeoSeries of Multi/Polygons and returns a list of Multi/Polygons
    '''
    new_geo = []
    for p in geometry:
        if p.has_z:
            if p.geom_type == 'Polygon':
                lines = [xy[:2] for xy in list(p.exterior.coords)]
                new_p = Polygon(lines)
                new_geo.append(new_p)
            elif p.geom_type == 'MultiPolygon':
                new_multi_p = []
                for ap in p:
                    lines = [xy[:2] for xy in list(ap.exterior.coords)]
                    new_p = Polygon(lines)
                    new_multi_p.append(new_p)
                new_geo.append(MultiPolygon(new_multi_p))
    return new_geo


df = gp.read_file("countyBoundaries.shp")
df.geometry = convert_3D_2D(df.geometry)
df.to_file("countyBoundaries2.shp")

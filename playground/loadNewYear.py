


f = open("USA_Counties_Format.csv","r")
f.readline()
lines = f.read().strip().split("\n")
f.close()


data = {}
for line in lines:
    line = line.strip()
    if line!="":
        parts = line.split(",")
        data[int(parts[0])] = parts

#print len(data)



f = open("data/CO-EST00INT-TOT.csv","r")
f.readline()
lines = f.read().strip().split("\n")
f.close()


newData = {}
for line in lines:
    line = line.strip()
    if line!="":
        parts = line.split(",")
        stateCode = parts[3]
        countyCode = "%03d" % int(parts[4])
        fullCode = int(stateCode+countyCode)

        if fullCode in data:
            newDataRow = data[fullCode]
            newDataRow[3] = parts[-1] #2010 estimated data
            newData[fullCode] = newDataRow
        else:
            print "Error " + str(fullCode) + " key not found"


keys = sorted(newData.keys())

f = open("USA_COUNTIES_2010.csv","w")
f.write("%d\n" % len(data))
for key in keys:
    f.write("%s\n" % (",".join(newData[key])))
f.close()

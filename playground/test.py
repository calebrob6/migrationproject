import numpy as np
import matplotlib.pyplot as plt


HEIGHT = 10
WIDTH = 10

mean = [0, 0]
cov = [[HEIGHT, 0], [0, WIDTH]]  # diagonal covariance

x, y = np.random.multivariate_normal(mean, cov, 5000).T

x = np.floor(x)
y = np.floor(y)

print x.shape, y.shape

grid = np.ones((HEIGHT,WIDTH))

for coord in zip(x,y):
    if 0<=coord[0]<HEIGHT and 0<=coord[1]<WIDTH:
        grid[coord]+=1

print grid

fig = plt.figure(figsize=(6, 6))

ax = fig.add_subplot(111)
ax.set_title('colorMap')
plt.imshow(grid)
ax.set_aspect('equal')

cax = fig.add_axes([0.12, 0.1, 0.78, 0.8])
cax.get_xaxis().set_visible(False)
cax.get_yaxis().set_visible(False)
cax.patch.set_alpha(0)
cax.set_frame_on(False)
plt.colorbar(orientation='vertical')
plt.show()


#plt.plot(x, y, 'x')
#plt.axis('equal')
#plt.show()

#!/usr/bin/env python
import sys,os
import time
import argparse

import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt

import scipy as sp
import scipy.spatial
import numpy as np
import networkx as nx
import cv2

from collections import defaultdict

def writeGrid(grid,fn):
    image = grid / (grid.max()/255.0)
    image = np.around(image)
    cv2.imwrite(fn,image)

def displayGrid(grid,fn=None,maxVal=5):
    fig = plt.figure(figsize=(6, 4))

    ax = fig.add_subplot(111)
    ax.set_title('colorMap')
    plt.imshow(grid)
    ax.set_aspect('equal')

    cax = fig.add_axes([0.12, 0.1, 0.78, 0.8])
    cax.get_xaxis().set_visible(False)
    cax.get_yaxis().set_visible(False)
    cax.patch.set_alpha(0)
    cax.set_frame_on(False)
    cbar = plt.colorbar(orientation='vertical')
    cbar.set_clim(0, maxVal)
    if fn is None:
        plt.show()
    else:
        plt.savefig(fn)
    plt.clf()
    plt.close()

def distance(a,b):
    return sp.spatial.distance.cityblock(a,b)

'''
dist needs to be a distance function that returns an integer
TODO: can this take floating point vals?
'''
def s_ij(grid,i,j, dist=distance):
    height,width = grid.shape
    r = dist(i,j)
    population = -grid[i] - grid[j]
    for a in range(-r,r+1):
        for b in range(-r,r+1):
            coord = (i[0]+a, i[1]+b)
            if dist(i,coord)<=r:
                if 0<=coord[0]<height and 0<=coord[1]<width:
                    population+=grid[coord]
    return population

def gridIterator(grid):
    it = np.nditer(grid, flags=['multi_index'])
    while not it.finished:
        yield (it[0], it.multi_index)
        it.iternext()

def showFlux(fluxRow,height,width):
    grid = np.zeros((height,width))
    for coord,value in fluxRow.items():
        grid[coord] = value
    displayGrid(grid)

def writeFlux(fluxRow,height,width,fn, maxVal):
    grid = np.zeros((height,width))
    for coord,value in fluxRow.items():
        grid[coord] = value
    displayGrid(grid, fn, maxVal)

def getMultivariateGrid(meanLocation,height,width, numSamples=5000):
    mean = meanLocation
    cov = [[height, 0], [0, width]]

    x, y = np.random.multivariate_normal(mean, cov, numSamples).T
    x = np.floor(x)
    y = np.floor(y)

    grid = np.ones((height,width))

    for coord in zip(x,y):
        if 0<=coord[0]<height and 0<=coord[1]<width:
            grid[coord]+=1

    return grid

def doArgs(argList, name):
    parser = argparse.ArgumentParser(description=name)

    parser.add_argument('-v', "--verbose", action="store_true", help="Enable verbose debugging", default=False)
    parser.add_argument('--input', action="store", dest="inputFn", type=str, help="Input file name", required=True)
    parser.add_argument('--output', action="store", dest="outputFn", type=str, help="Output file name", required=True)

    return parser.parse_args(argList)

def main():
    progName = "Radiation Model"
    args = doArgs(sys.argv[1:], progName)

    verbose = args.verbose
    inputFn = args.inputFn
    outputFn = args.outputFn

    print "Starting %s" % (progName)
    startTime = float(time.time())

    if not os.path.isfile(inputFn):
        print "Input doesn't exist, exiting"
        return

    outputBase = os.path.dirname(outputFn)
    if outputBase!='' and not os.path.exists(outputBase):
        print "Output directory doesn't exist, making output dirs: %s" % (outputBase)
        os.makedirs(outputBase)


    WIDTH = 7
    HEIGHT = 7
    #grid = np.random.randint(1, high=100, size=(HEIGHT,WIDTH))
    grid = getMultivariateGrid((0,0),HEIGHT,WIDTH)
    grid = np.array(grid, dtype=float)

    N = np.sum(grid)
    Nc = 0.5 * N

    TIME_STEPS = 1
    iterationTime = float(time.time())
    for iterNum in xrange(TIME_STEPS):
        if iterNum%10==0:
            print "%d/%d\t%0.4f seconds" % (iterNum+1,TIME_STEPS, time.time()-iterationTime)
            iterationTime = float(time.time())

        Ti = np.zeros((HEIGHT,WIDTH))
        for popval,coord in gridIterator(grid):
            Ti[coord] = popval*(Nc/N)

        maxVal = float('-inf')
        Tij = defaultdict(dict)
        for mi,i in gridIterator(grid):
            print i
            for nj,j in gridIterator(grid):
                if i!=j:
                    sij = s_ij(grid,i,j)
                    outputVal = Ti[i] * ((mi*nj)/((mi+sij)*(mi+nj+sij)))
                    maxVal = max(maxVal,outputVal)
                    Tij[i][j] = outputVal

        print maxVal
        #displayGrid(grid)

        for k in Tij.keys():
            fn = os.path.join(outputBase,"%d_%d.png" % (k[0],k[1]))
            writeFlux(Tij[k], HEIGHT, WIDTH, fn, maxVal)

        #showFlux(Tij[(9,9)], HEIGHT, WIDTH)

        f = open(os.path.join(outputBase,"%d.csv" % (iterNum)),"w")
        keyList = sorted(Tij.keys())
        for k1 in keyList:
            for k2 in keyList:
                if k1 in Tij and k2 in Tij[k1]:
                    f.write(str(Tij[k1][k2])+",")
                else:
                    f.write(str(0)+",")
            f.write("\n")
        f.close()

        writeGrid(grid,os.path.join(outputBase,"%d.png" % (iterNum)))

    print "Finished in %0.4f seconds" % (time.time() - startTime)
    return


def test():
    population = np.ones((10,10))
    assert s_ij(population, (5,5), (5,7))==11.0
    return True

if __name__ == '__main__':
    sys.argv = ["programName.py", "--input", "input/data.txt", "--output", "output/test.txt"]

    if test():
        main()

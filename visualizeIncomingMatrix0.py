#!/usr/bin/env python
import sys,os
import time
import argparse

import matplotlib
import matplotlib.pyplot as plt

from matplotlib.colors import Normalize
from matplotlib.collections import PatchCollection

from mpl_toolkits.basemap import Basemap
from shapely.geometry import Point, Polygon, MultiPoint, MultiPolygon
from shapely.prepared import prep

from pysal.esda.mapclassify import Natural_Breaks as nb
from pysal.esda.mapclassify import Maximum_Breaks as mb
from pysal.esda.mapclassify import User_Defined

from descartes import PolygonPatch

import fiona
import stateplane
import pandas as pd
import numpy as np
import networkx as nx
import natsort
import itertools

from Data import *

def doArgs(argList, name):
    parser = argparse.ArgumentParser(description=name)

    parser.add_argument('-v', "--verbose", action="store_true", help="Enable verbose debugging", default=False)
    parser.add_argument('--inputMatrix', action="store", dest="inputMatrix", type=str, help="Input matrix", required=True)
    parser.add_argument('--output', action="store", dest="outputFn", type=str, help="Output file name", required=True)
    parser.add_argument('--year', action="store", dest="year", type=int, help="Output year", required=True)
    return parser.parse_args(argList)

def main():
    progName = "Visualize County Migration/Population Fractions"
    args = doArgs(sys.argv[1:], progName)

    verbose = args.verbose
    inputMatrix = args.inputMatrix
    outputFn = args.outputFn
    year = args.year

    print "Starting %s" % (progName)
    startTime = float(time.time())


    if not os.path.exists(inputMatrix):
        print "Input doesn't exist, exiting..."
        return

    outputBase = os.path.dirname(outputFn)
    if outputBase!='' and not os.path.exists(outputBase):
        print "Output directory doesn't exist, making output dirs: %s" % (outputBase)
        os.makedirs(outputBase)



    startYear = 2004
    stopYear = 2010

    print "Visualizing from %d to %d (inclusive)" % (startYear, stopYear)

    yearRange = range(startYear,stopYear+1)
    migrationDataFiles = [
        "data/output/groundTruthOutput/%d_groundTruth_data.csv" % (i) for i in yearRange
    ]
    migrationDataSets = [loadMatrix(fn) for fn in migrationDataFiles]

    #zero out the self migration values
    for i in range(len(migrationDataSets)):
        for j in range(migrationDataSets[i].shape[0]):
            migrationDataSets[i][j,j] = 0

    #load a list of county fips id's in sorted order
    countyIds = loadCounties()

    data = [
        {
            countyId:np.sum(migrationDataSets[i][:,j])
            for j,countyId in enumerate(countyIds)
        }
        for i in range(len(migrationDataSets))
    ]

    colorBarMin,colorBarMax = float('inf'),float('-inf')
    colorBarMinCounty,colorBarMaxCounty = 0,0
    for d in data:
        for k,v in d.items():
            if v>colorBarMax:
                colorBarMax = v
                colorBarMaxCounty = k
            if v<colorBarMin:
                colorBarMin = v
                colorBarMinCounty = k
    print colorBarMin,colorBarMinCounty
    print colorBarMax,colorBarMaxCounty
    #colorBarMax=0.65

    shp = fiona.open("data/countyBoundaries/countyBoundaries2.shp")
    bds = shp.bounds
    shp.close()

    ll = (bds[0], bds[1])
    ur = (bds[2], bds[3])
    coords = list(itertools.chain(ll, ur))
    w, h = coords[2] - coords[0], coords[3] - coords[1]

    lat = (24.396308, 49.384358)
    lon = (-124.848974, -66.885444)


    for i in range(len(yearRange)):
        year = yearRange[i]
        print "Calculating the colormap for %d" % (year)

        fig = plt.figure()
        ax = fig.add_subplot(111, axisbg='#ffffff', frame_on=False)

        m = Basemap(
            projection='merc',
            llcrnrlat=lat[0],
            urcrnrlat=lat[1],
            llcrnrlon=lon[0],
            urcrnrlon=lon[1],
            resolution=None,
            fix_aspect=False,
            suppress_ticks=True
        )

        m.readshapefile(
            "data/countyBoundaries/countyBoundaries2",
            "dataInfo",
            drawbounds = False
        )


        polyList = []
        fipsList = []
        dataList = []
        for xy, info in zip(m.dataInfo,m.dataInfo_info):
            idVal = int(info["STATEFP"]+info["COUNTYFP"])
            if idVal in countyIds:
                fipsList.append(idVal)
                polyList.append(Polygon(xy))
                dataList.append(data[i][idVal])

        df_map = pd.DataFrame(
            {
            'poly': polyList,
            'cnt_fip': fipsList,
            'dataPoint': dataList
            }
        )

        df_map['area_m'] = df_map['poly'].map(lambda x: x.area)

        cmap = plt.get_cmap('Blues')

        df_map['patches'] = df_map['poly'].map(lambda x: PolygonPatch(x, ec='#555555', lw=.2, alpha=1.0, zorder=4))

        pc = PatchCollection(df_map['patches'], match_original=True)

        norm = Normalize(vmin=colorBarMin, vmax=colorBarMax)
        faceColorValues = cmap(norm(df_map['dataPoint'].values))
        pc.set_facecolor(faceColorValues)
        ax.add_collection(pc)

        scalarMap = matplotlib.cm.ScalarMappable(cmap=cmap,norm=norm)
        scalarMap.set_clim(colorBarMin, colorBarMax)
        scalarMap.set_array(df_map['dataPoint'].values)

        colorBar = plt.colorbar(
            scalarMap,
            shrink=0.9,
            orientation='vertical',
            pad=0.05
        )

        m.drawmapscale(
            lon[0]+6, lat[0]+1.5, lon[0], lat[0],
            1000,
            barstyle='fancy',
            labelstyle='simple',
            fillcolor1='#ffffff',
            fillcolor2='#555555',
            fontcolor='#555555',
            zorder=5
        )

        m.drawmapboundary(
            color='k',
            linewidth=0.0,
            fill_color='#ffffff',
            zorder=None,
            ax=ax
        )

        plt.title("Migrants/Population Fractions, %d" % (year), fontsize=26)

        plt.tight_layout()
        fig.set_size_inches(22, 10)
        plt.savefig(os.path.join(outputFn,"output_%d.png" % (year)), dpi=100, alpha=True)

    print "Finished in %0.4f seconds" % (time.time() - startTime)
    return

if __name__ == '__main__':
    sys.argv = ["programName.py","--inputMatrix","data/output/groundTruthOutput/2008_groundTruth_data.csv","--year","2008","--output","outputs/"]
    main()

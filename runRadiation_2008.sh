#!/bin/bash

for i in `seq 0.1 0.1 1`;
do
  echo "Comparing 2008 data with c=$i"
  python radiationModel.py --input data/output/population/2008_population_data.csv --output data/output/radiationOutput/2008_c_${i}_output_data.csv --c $i
  python compareODMatrices.py --input1 data/output/groundTruthOutput/2008_groundTruth_data.csv --input2 data/output/radiationOutput/2008_c_${i}_output_data.csv
  echo "-----------------------------------------------------------"
done

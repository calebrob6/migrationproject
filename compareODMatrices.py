#!/usr/bin/env python
import sys,os
import time
import argparse

import numpy as np
from scipy import stats


def doCPC(T,Tprime):
    numerator = np.sum(np.minimum(T,Tprime))
    denominator = np.sum(T+Tprime)
    return  (2*float(numerator)/float(denominator))

def doArgs(argList, name):
    parser = argparse.ArgumentParser(description=name)

    parser.add_argument('-v', "--verbose", action="store_true", help="Enable verbose debugging", default=False)
    parser.add_argument('--input1', action="store", dest="inputFn1", type=str, help="Input Ground Truth file name", required=True)
    parser.add_argument('--input2', action="store", dest="inputFn2", type=str, help="Input Radiation Output file name", required=True)
    parser.add_argument('--method', action="store", dest="method", type=str, help="Method to use, choices are cpc or r2", required=False, default="cpc")
    return parser.parse_args(argList)

def main():
    progName = "Radiation Output Evaluation"
    args = doArgs(sys.argv[1:], progName)

    verbose = args.verbose
    inputFn1 = args.inputFn1
    inputFn2 = args.inputFn2
    method = args.method

    print "Starting %s" % (progName)
    startTime = float(time.time())

    if not os.path.isfile(inputFn1) or not os.path.isfile(inputFn2):
        print "Input doesn't exist, exiting"
        return

    print "Loading input files"
    data1 = np.loadtxt(inputFn1, dtype=int, delimiter=",", skiprows=0)
    data2 = np.loadtxt(inputFn2, dtype=int, delimiter=",", skiprows=0)

    n = data1.shape[1]
    assert data1.shape[0] == data2.shape[0] and data1.shape[1] == data2.shape[1]

    for i in xrange(n):
        data1[i,i] = 0
        data2[i,i] = 0

    if method=="cpc":
        print "Calculating CPC value"
        print doCPC(data1,data2)
    elif method=="r2":
        print "Calculating rSquared values"
        rSquaredVals = np.zeros(n)
        for i in xrange(1,data1.shape[0]):
            slope, intercept, r_value, p_value, std_err = stats.linregress(data1[i,:],data2[i,:])
            rSquaredVals[i-1] = r_value**2

        print rSquaredVals.mean()
        print rSquaredVals.std()
        print rSquaredVals.max()
        print rSquaredVals.min()

        '''
        #Show which r2 value is really low... It is in Nevada...
        for i,k in enumerate(data1[0,:]):
            if rSquaredVals[i] < 0.4:
                print k,rSquaredVals[i]
        '''
    else:
        print "Error, invalid method"



    '''
    if verbose:
        import matplotlib.pyplot as plt
        import matplotlib.mlab as mlab

        mu, sigma = rSquaredVals.mean(), rSquaredVals.std()
        x = mu + sigma*np.random.randn(10000)

        # the histogram of the data
        weights = np.ones_like(rSquaredVals)/float(len(rSquaredVals))
        #n, bins, patches = plt.hist(rSquaredVals, 50, normed=False, weights=weights)
        n, bins, patches = plt.hist(rSquaredVals, 50, normed=False)
        # add a 'best fit' line
        #y = mlab.normpdf( bins, mu, sigma)
        #l = plt.plot(bins, y, 'r--', linewidth=1)

        plt.xlabel('r^2 values')
        plt.ylabel('Frequency')
        plt.title(r'Histogram of r^2 values')
        plt.grid(True)

        plt.show()
    '''

    print "Finished in %0.4f seconds" % (time.time() - startTime)
    return

if __name__ == '__main__':
    #sys.argv = ["programName.py","--input","test.txt","--output","tmp/test.txt"]
    main()

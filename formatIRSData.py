#!/usr/bin/env python
import sys,os
import time
import argparse

import numpy as np
from Data import *

def doArgs(argList, name):
    parser = argparse.ArgumentParser(description=name)

    parser.add_argument('-v', "--verbose", action="store_true", help="Enable verbose debugging", default=False)
    parser.add_argument('--input', action="store", dest="inputFn", type=str, help="Input Year", required=True)
    parser.add_argument('--output', action="store", dest="outputFn", type=str, help="Output file name", required=True)

    return parser.parse_args(argList)

def main():
    progName = "Format IRS Data"
    args = doArgs(sys.argv[1:], progName)

    verbose = args.verbose
    inputFn = args.inputFn
    outputFn = args.outputFn

    print "Starting %s" % (progName)
    startTime = float(time.time())

    outputBase = os.path.dirname(outputFn)
    if outputBase!='' and not os.path.exists(outputBase):
        print "Output directory doesn't exist, making output dirs: %s" % (outputBase)
        os.makedirs(outputBase)


    countyIds = loadCounties()

    dataFrame = loadFile(year=int(inputFn))

    f = open(outputFn,"w")
    headerLine = ','.join(map(str,countyIds))
    f.write("%s\n"%(headerLine))
    for i,_ in enumerate(countyIds):
        outputLine = []
        for j,__ in enumerate(countyIds):
            outputLine.append(int(dataFrame.data[i][j]))
        assert len(outputLine)==3143
        f.write("%s\n" % (','.join(map(str,outputLine))))
    f.close()


    print "Finished in %0.4f seconds" % (time.time() - startTime)
    return

if __name__ == '__main__':
    main()

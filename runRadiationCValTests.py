
import numpy as np
import subprocess
import os
import time
import sys

def main():

    yearVal = int(sys.argv[1])

    startTime = float(time.time())
    f=open("radiationCValTests_%d_output.csv" % (yearVal),"w")
    f.write("cVal,mean,std,max,min\n")
    testVals = np.arange(0.01,1,0.01)
    n = len(testVals)
    for i,c in enumerate(testVals):
        print "%d\t%d/%d - %0.2f - %0.4f" % (yearVal,i+1,n,c,time.time()-startTime)

        radiationExecute = [
            "python",
            "radiationModel.py",
            "--input",
            "data/output/population/%d_population_data.csv" % (yearVal),
            "--output",
            "data/output/radiationOutput/%d_c_%0.2f_output_data.csv" % (yearVal,c),
            "--c",
            "%0.2f" % (c)
        ]

        compareExecute = [
            "python",
            "compareODMatrices.py",
            "--input1",
            "data/output/groundTruthOutput/%d_groundTruth_data.csv" % (yearVal),
            "--input2",
            "data/output/radiationOutput/%d_c_%0.2f_output_data.csv" % (yearVal,c)
        ]

        #proc = subprocess.Popen(radiationExecute,stdout=subprocess.PIPE)
        #proc.wait()
        proc = subprocess.Popen(compareExecute,stdout=subprocess.PIPE)

        lines = []
        for line in iter(proc.stdout.readline,''):
           lines.append(line.strip())

        #f.write("%0.2f,%s,%s,%s,%s\n" % (c,lines[3],lines[4],lines[5],lines[6]))
        f.write("%0.2f,%s\n" % (c,lines[3]))
        f.flush()
        os.fsync(f)

    f.close()
if __name__ == '__main__':
    main()

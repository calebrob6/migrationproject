import csv
from collections import defaultdict
import numpy as np
import os

class MigrationYear(object):

    def __init__(self, countyList):
        self.n = len(countyList)
        self.data = {_:{__:None for __ in countyList} for _ in countyList}
        self.countyList = countyList
        #self.data = np.zeros((n,n))

    def finalize(self):
        self.newData = np.zeros((self.n,self.n))
        for i,county1 in enumerate(self.countyList):
            for j,county2 in enumerate(self.countyList):
                if self.data[county1][county2]!=None:
                    self.newData[i,j] = self.data[county1][county2]
                else:
                    self.newData[i,j] = 0
        self.data = self.newData
        del self.countyList

def loadPopulationFile(fn):
    f = open(fn,"r")
    f.readline()
    lines = f.read().strip().split("\n")
    f.close()
    data = {}
    totalPopulation = 0
    for line in lines:
        line = line.strip()
        if line!="":
            parts = line.split(",")
            countyPopulation = int(parts[3])
            totalPopulation += countyPopulation
            data[int(parts[0])] = countyPopulation


    n = len(data)
    keys = sorted(data.keys()) #we use this to iterate over the dictionary to make sure we go through it the same way each time.
    data2 = {}
    for i,key in enumerate(keys):
        data2[i] = data[key]
    return data2

def parseFixedLengthRow(line):
    #State_Code_Origin, County_Code_Origin, State_Code_Dest, County_Code_Dest, State_Abbrv, County_Name, Return_Num, Exmpt_Num, Aggr_AGI
    #00 000 96 000 US Total Mig - US & For               7238512   13719599   314723861    24833
    returnDict = dict()

    returnDict["State_Code_Origin"] = line[0:2].strip()
    returnDict["County_Code_Origin"] = line[3:6].strip()
    returnDict["State_Code_Dest"] = line[7:9].strip()
    returnDict["County_Code_Dest"] = line[10:13].strip()
    returnDict["State_Abbrv"] = line[14:16].strip()
    returnDict["County_Name"] = line[17:49].strip()
    returnDict["Return_Num"] = line[50:59].strip()
    returnDict["Exmpt_Num"] = line[60:70].strip()
    returnDict["Aggr_AGI"] = line[71:82].strip()

    return returnDict


def loadFile04_08(fnIn, fnOut, countyList):
    migrationStruct = MigrationYear(countyList)

    with open(fnIn,"r") as f:
        for line in f:
            line = line.strip()
            if line!="":
                row = parseFixedLengthRow(line)
                fieldName  = row["County_Name"]
                val = int(row["Exmpt_Num"])

                origin = int("%02d%03d" % (int(row["State_Code_Origin"]), int(row["County_Code_Origin"])))
                destination = int("%02d%03d" % (int(row["State_Code_Dest"]), int(row["County_Code_Dest"])))

                if origin in countyList and destination in countyList:
                    i, j = origin, destination
                    if migrationStruct.data[i][j]!=None and migrationStruct.data[i][j]!=val:
                        raise ValueError("%d, %d" % (origin, destination))
                    else:
                        migrationStruct.data[i][j] = val

    with open(fnIn,"r") as f:
        for line in f:
            line = line.strip()
            if line!="":
                row = parseFixedLengthRow(line)
                fieldName  = row["County_Name"]
                val = int(row["Exmpt_Num"])

                origin = int("%02d%03d" % (int(row["State_Code_Origin"]), int(row["County_Code_Origin"])))
                destination = int("%02d%03d" % (int(row["State_Code_Dest"]), int(row["County_Code_Dest"])))

                if origin in countyList and destination in countyList:
                    i, j = origin, destination
                    if migrationStruct.data[i][j]!=None and migrationStruct.data[i][j]!=val:
                        raise ValueError("%d, %d" % (origin, destination))
                    else:
                        migrationStruct.data[i][j] = val

    migrationStruct.finalize()
    return migrationStruct

def loadFile08_10(fnIn, fnOut, countyList):

    migrationStruct = MigrationYear(countyList)

    with open(fnIn,"r") as f:
        reader = csv.DictReader(f)
        for row in reader:
            fieldName  = row["County_Name"]
            val = int(row["Exmpt_Num"])

            origin = int("%02d%03d" % (int(row["State_Code_Origin"]), int(row["County_Code_Origin"])))
            destination = int("%02d%03d" % (int(row["State_Code_Dest"]), int(row["County_Code_Dest"])))

            if origin in countyList and destination in countyList:
                i, j = origin, destination
                if migrationStruct.data[i][j]!=None and migrationStruct.data[i][j]!=val:
                    raise ValueError("%d, %d" % (origin, destination))
                else:
                    migrationStruct.data[i][j] = val

    with open(fnOut,"r") as f:
        reader = csv.DictReader(f)
        for row in reader:
            fieldName  = row["County_Name"]
            val = int(row["Exmpt_Num"])

            origin = int("%02d%03d" % (int(row["State_Code_Origin"]), int(row["County_Code_Origin"])))
            destination = int("%02d%03d" % (int(row["State_Code_Dest"]), int(row["County_Code_Dest"])))

            if origin in countyList and destination in countyList:
                i, j = origin, destination
                if migrationStruct.data[i][j]!=None and migrationStruct.data[i][j]!=val:
                    raise ValueError("%d, %d" % (origin, destination))
                else:
                    migrationStruct.data[i][j] = val

    migrationStruct.finalize()
    return migrationStruct

def loadFile11_13(fnIn, fnOut, countyList):

    migrationStruct = MigrationYear(countyList)

    with open(fnIn,"r") as f:
        reader = csv.DictReader(f)
        for row in reader:
            val = int(row["n2"])

            origin = int("%02d%03d" % (int(row["y1_statefips"]), int(row["y1_countyfips"])))
            destination = int("%02d%03d" % (int(row["y2_statefips"]), int(row["y2_countyfips"])))

            if origin in countyList and destination in countyList:
                i, j = origin, destination
                if migrationStruct.data[i][j]!=None and migrationStruct.data[i][j]!=val:
                    raise ValueError("%d, %d" % (origin, destination))
                else:
                    migrationStruct.data[i][j] = val

    with open(fnOut,"r") as f:
        reader = csv.DictReader(f)
        for row in reader:
            val = int(row["n2"])

            origin = int("%02d%03d" % (int(row["y1_statefips"]), int(row["y1_countyfips"])))
            destination = int("%02d%03d" % (int(row["y2_statefips"]), int(row["y2_countyfips"])))

            if origin in countyList and destination in countyList:
                i, j = origin, destination
                if migrationStruct.data[i][j]!=None and migrationStruct.data[i][j]!=val:
                    raise ValueError("%d, %d" % (origin, destination))
                else:
                    migrationStruct.data[i][j] = val

    migrationStruct.finalize()
    return migrationStruct

def loadCounties():
    f = open("data/input/countyList.csv","r")
    f.readline()
    lines = f.read().strip().split("\n")
    f.close()
    countyIds = []
    for line in lines:
        parts = line.strip().split(",")
        countyId = int(parts[0])
        countyIds.append(countyId)

    countyIds = sorted(countyIds)
    return countyIds


def loadCountyNameMap():
    countyIdMap = dict()
    f = open("data/input/national_county.txt","r")
    lines = f.read().strip().split("\n")
    f.close()

    for line in lines:
        line = line.strip()
        if line!="":
            parts = line.split(",")
            idVal = int(parts[1]+parts[2])
            name = parts[3]
            state = parts[0]
            countyIdMap[idVal] = name+", "+state
    return countyIdMap

def loadFile(year=2004):

    countyList = loadCounties()

    if year==2004:
        return loadFile04_08("data/input/2004_2005/countyin0405us1.dat","data/input/2004_2005/countyout0405us1.dat", countyList)
    elif year==2005:
        return loadFile04_08("data/input/2005_2006/countyin0506.dat","data/input/2005_2006/countyout0506.dat", countyList)
        print "This data is broken..."
    elif year==2006:
        return loadFile04_08("data/input/2006_2007/countyin0607.dat","data/input/2006_2007/countyout0607.dat", countyList)
    elif year==2007:
        return loadFile04_08("data/input/2007_2008/ci0708us.dat","data/input/2007_2008/co0708us.dat", countyList)
    elif year==2008:
        return loadFile08_10("data/input/2008_2009/countyinflow0809.csv","data/input/2008_2009/countyoutflow0809.csv", countyList)
    elif year==2009:
        return loadFile08_10("data/input/2009_2010/countyinflow0910.csv","data/input/2009_2010/countyoutflow0910.csv", countyList)
    elif year==2010:
        return loadFile08_10("data/input/2010_2011/countyinflow1011.csv","data/input/2010_2011/countyoutflow1011.csv", countyList)
    elif year==2011:
        return loadFile11_13("data/input/2011_2012/countyinflow1112.csv","data/input/2011_2012/countyoutflow1112.csv", countyList)
    elif year==2012:
        return loadFile11_13("data/input/2012_2013/countyinflow1213.csv","data/input/2012_2013/countyoutflow1213.csv", countyList)
    elif year==2013:
        return loadFile11_13("data/input/2013_2014/countyinflow1314.csv","data/input/2013_2014/countyoutflow1314.csv", countyList)
    else:
        raise Exception("Year invalid")

    print "Something wrong happened?"


def loadMatrix(fn):
    return np.loadtxt(fn, dtype=int, delimiter=",", skiprows=1)


def loadMigrationPopulationDataset(yearRange,zeroSelfMigration=True):

    populationDataFiles = [
        "data/output/population/%d_population_data.csv" % (i) for i in yearRange
    ]
    migrationDataFiles = [
        "data/output/groundTruthOutput/%d_groundTruth_data.csv" % (i) for i in yearRange
    ]

    populationDataSets = [loadPopulationFile(fn) for fn in populationDataFiles]
    migrationDataSets = [loadMatrix(fn) for fn in migrationDataFiles]


    if zeroSelfMigration:
        #zero out the self migration values
        for i in range(len(migrationDataSets)):
            for j in range(migrationDataSets[i].shape[0]):
                migrationDataSets[i][j,j] = 0
    else:
        pass

    return populationDataSets,migrationDataSets


def loadCountyPairwiseDistanceMatrix():
    if not os.path.exists("data/outputDistanceMatrix.npy"):
        print "Calculating distance matrix"

        import scipy.spatial.distance

        f = open("data/output/population/2008_population_data.csv","r")
        f.readline()
        lines = f.read().strip().split("\n")
        f.close()

        data = {}
        for line in lines:
            line = line.strip()
            if line!="":
                parts = line.split(",")
                countyPopulation = int(parts[3])
                data[int(parts[0])] = [countyPopulation] + parts
        n = len(data)
        keys = sorted(data.keys())

        coords = [(float(data[k][5]),float(data[k][6])) for k in keys]
        distanceMatrix = scipy.spatial.distance.cdist(coords,coords,haversine)
        np.save("outputDistanceMatrix.npy",distanceMatrix)
    else:
        print "Loading the distance matrix"
        distanceMatrix = np.load("data/outputDistanceMatrix.npy")

    return distanceMatrix

def loadCountyCentroids():
    f = open("data/input/visualizerOutputFormat.csv","r")
    f.readline()
    lines = f.read().strip().split("\n")
    f.close()

    data = {}
    for line in lines:
        line = line.strip()
        if line!="":
            parts = line.split(",")
            data[int(parts[0])] = parts
    keys = sorted(data.keys())
    coords = [(float(data[k][4]),float(data[k][5])) for k in keys]
    return coords

if __name__ == '__main__':
    data = loadFile(2011).data
    countyList = loadCounties()
    i = countyList.index(20201)
    print data[i, data[i,:]!=0]
    print data.shape

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notes\n",
    "\n",
    "This is a collection of project notes for the Migration Modeling project.\n",
    "\n",
    "<h2 id=\"tocheading\">Table of Contents</h2>\n",
    "<div id=\"toc\"></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Digital Elevation Model (DEM) Data\n",
    "\n",
    "The following two sections describe the two different data sources we can use to model sea level rise.\n",
    "\n",
    "## SRTMGL30\n",
    "\n",
    "This dataset is the SRTMGL30 dataset. It can be downloaded [here](http://e4ftl01.cr.usgs.gov/SRTM/SRTMGL30.002/2000.02.11/) and is described [here](https://lpdaac.usgs.gov/dataset_discovery/measures/measures_products_table/srtmgl30_v021).\n",
    "\n",
    "The data is stored in binary format as 16 bit signed integers in row-major order with no header information. The data is chunked up into 27 different files where the individual filenames look like \"E020N40.DEM\" or \"W180S10.DEM\" and encode the latitude and longitude point of the upper left corner of the file. I have written a class to read these \".DEM\" files, a routine to read in all the files, arrange them together and save them all as one giant matrix, and lastly a routine to convert the matrix into a geotiff raster file under the WGS84 coordinate system so that I can manipulate the data with GIS libraries.\n",
    "\n",
    "This paper should be cited with the SRTM data, [A Global Assessment of the SRTM Performance](http://www.asprs.org/a/publications/pers/2006journal/march/2006_mar_249-260.pdf).\n",
    "\n",
    "\n",
    "## Using NOAA Digital Coast Data\n",
    "\n",
    "The Digital Coast SLR Viewer tool can be found [here](https://coast.noaa.gov/digitalcoast/tools/slr/), with data downloads availiable [here](https://coast.noaa.gov/slrdata/). The data includes DEM raster IMG files at a 3 meter resolution along the coast and ESRI File Geodatabase sets that contain polygons of land affected by 0-6ft of sea level rise.\n",
    "\n",
    "The FAQ for the tool can be found [here](https://coast.noaa.gov/digitalcoast/_/pdf/SLRViewerFAQ.pdf).\n",
    "\n",
    "I am using the following code to scrape the download URLS for the polygon and DEM data:\n",
    "\n",
    "```\n",
    "    $(\"li[ng-show='slr.slrurl']\").find(\"a\").each(function(){\n",
    "        console.debug($(this).attr(\"href\"));\n",
    "    });\n",
    "    $(\"li[ng-show='slr.demurl']\").find(\"a\").each(function(){\n",
    "        console.debug($(this).attr(\"href\"));\n",
    "    });\n",
    "```\n",
    "\n",
    "I manually save the list of URLs to a text file, then use ```wget -i fileList.txt``` to download the list of zip files.\n",
    "\n",
    "This results in 49 files with a total compressed size of 35 GB.\n",
    "\n",
    "Note, this paragraph was taken from the FAQ regarding the DEM creation process:\n",
    "> DEMs are created by first gathering the best available lidar-based elevation data (see data accuracy question). This includes raw and/or bare earth lidar data, digitized breaklines (where available), National Hydrography Dataset boundaries, National Wetland Inventory boundaries, levee centerlines, and elevations. Not all these sources are used in a single DEM, but rather a combination is used where data gaps exist to adequately condition the DEM for mapping purposes. Conditioning the DEM means including hydro-features and breaklines in the DEM to make sure areas that currently experience flooding are depicted accurately. For example, bridges are removed so breaks in the DEM will not separate water bodies in the final map product. The specifications for hydro-flattening the DEMs for sea level rise (SLR) mapping are more stringent than other national specifications (e.g., USGS) in that smaller hydrologic features are incorporated into the DEM (Any hydro-feature larger than 10 meters or ~30 feet is included). Because these DEMs are created specifically for SLR mapping, they may not be appropriate to use for other applications, such as FEMA floodplain boundary mapping. For example, water feature elevations have been lowered to -1 feet for mapping purposes.\n",
    "\n",
    "## Other Alternatives\n",
    "\n",
    "The OpenStreetMap wiki contains a list of other DEM data sources, available [here](http://wiki.openstreetmap.org/wiki/SRTM). The best alternative to our current approach is the higher resolution versions of SRTMGL30, which are available in 1 and 3 arc second resolutions (instead of the current 30 arc second resolution).\n",
    "\n",
    "The following are download commands I made for other datasets:\n",
    "\n",
    "```bash\n",
    "wget -r -l1 -H -t1 -nd -N -np -A.zip http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/North_America/\n",
    "wget -r -l1 -H -t1 -nd -N -np -A.jpg http://e4ftl01.cr.usgs.gov/SRTM/SRTMGL3.003/2000.02.11/\n",
    "wget -r -l1 -H -t1 -nd -N -np -A.zip http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/North_America/\n",
    "```\n",
    "\n",
    "## Processing Raster Vector Masking\n",
    "\n",
    "I'm currently using the [rasterio library](https://github.com/mapbox/rasterio/) to perform a raster by vector mask of the digital elevation model in order to determine which pixels lie in a given county. I have verified that everything is working correctly by importing the geotiff raster, the county shapefile, and a sample of the individually masked counties into QGIS and checking that everything lines up accordingly (which should happen because everything is using the same WGS84 coordinate system). By doing this I caught a bug in my initial matrix to geotiff raster implementation.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Floodfill Implementation\n",
    "\n",
    "To make sure that the calculation on what areas area affected by sea level rise does not include lakes or other \"interior\" points, we perform a flood fill starting in the ocean that covers up to $x$ meters. This creates a mask where `1`s represent the land that is reachable by a $x$ meter sea level rise and `0`s represent the land that isn't (although may still be lower than $x$ in some places)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Figure Descriptions for Paper\n",
    "\n",
    "## Figure 1\n",
    "\n",
    "This describes the direct effects of sea level rise on the population and land.\n",
    "\n",
    "### Subplot 1\n",
    "\n",
    "Show how percent amount of land covered by water varies with sea level rise amount.\n",
    "\n",
    "### Subplot 2\n",
    "\n",
    "Show how percent amount of population displaced by land varies with sea level rise amount.\n",
    "\n",
    "### Subplot 3\n",
    "\n",
    "Show how the population density varies with sea level rise amount.\n",
    "\n",
    "\n",
    "## Figure 2\n",
    "\n",
    "Show how the radiation model works compared to the IRS ground truth data. This can be a visualization of several rows from the `T` matrix. This might contain more than 1 example. Compare to gravity model as well (depends on whether we want to include the reasoning for choosing the radiation model)?\n",
    "\n",
    "## FIgure 3\n",
    "\n",
    "Show 3-4 scenarios with population distribution diffs that capture the effect of sea level rise on the wider national community (not just communities on the coast).\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# IRS Data\n",
    "\n",
    "The data that we are using for this project is the county-to-county migration data provided by the IRS, found [here](https://www.irs.gov/uac/SOI-Tax-Stats-Migration-Data). For each year in the range from 2004 to 2014 the IRS provides excel files for each state which contain outgoing and incoming migrations for each county in that state, as well as aggregate \"outgoing\" and \"incoming\" files that record the number of outgoing and incoming migrants for all counties. The datasets from years earlier than 2004 do not contain the summary files and would therefore take more effort to process, so we ignore them. We call the data files from the IRS *raw data*.\n",
    "\n",
    "The *raw data* aggregate files from the IRS come in 3 different formats:\n",
    "\n",
    "1. The datasets from 2004-2008 are in fixed width text format. I manually determined the width of each column in characters and wrote a parser function that deals with these files.\n",
    "2. The datsets from 2008-2010 come in both the fixed width format, and in CSV format. The column headers are the same as the 2004-2008 data, a different parser function deals with these files.\n",
    "3. The datasets from 2010-2014 only contain the aggregate files in CSV format, however use different column headers, so I have another parser function for these.\n",
    "\n",
    "We need to convert this raw data to a matrix format where an entry $i,j$ represents the number of migrants that went from county $i$ to county $j$ for each year. To do this we perform the following steps:\n",
    "\n",
    "1. Load a list of county ids that we care about from a master file (this is `countyList.csv` in our dataset), any county id that is not in this list will be ignored in the following steps (this prunes out the special \"others\" and migration totals lines). \n",
    "1. Given an outgoing and incoming file we create a matrix $T$ then iterate through each file line by line using the appropriate parser function described above.\n",
    "2. In both files we get the destination county id for a particular row by taking the State_Code_Dest and County_Code_Dest entries, left padding the County_Code_Dest with zeros until it is 3 characters long, and then appending the County_Code_Dest to the State_Code_Dest (e.g. a State_Code_Dest of '1' and a County_Code_Dest of '2' would become '1002')\n",
    "3. We perform the same steps to get the origin county id.\n",
    "4. We let the origin county id be $i$, and the destination county id be $j$, then set $T_{i,j}$ to be the Exmpt_Num value for that row. All the datasets are correctly set up so that a value $i,j$ in the countyout file will be $j,i$ in the countyin file, so we do not have to worry about reversing the indices in one or the other. The reason we use the Exmpt_Num value is because it represents the number of people, while Return_Num represents the number of families.\n",
    "5. Finally, we write the matrix $T$ to file where the first line is a header line that gives the county ids in the order that they are represented in the matrix (e.g. if the first entry in the header line is '1002' and the second entry is '1004' then the entry $T_{1,2}$ will represent the number of outgoing migrants from '1002' to '1004'.\n",
    "\n",
    "## Missing 2005-2006 Summary Data\n",
    "\n",
    "I noticed that the 2005-2006 dataset from the IRS website is missing data in the countyin0506.dat and countyout0506.dat files. Instead of having rows for all rows from the individual state's files like the rest of the datasets, these files only contain several summary lines. I sent the following message to SIS@irs.gov:\n",
    "\n",
    "> First question is: the aggregate 2005-2006 County-to-County data files (https://www.irs.gov/pub/irs-soi/county0506.zip) seem to be broken. The countyin0506.dat and countyout0506.dat files do not contain all the entries like the other datasets, even though the individual state files seem to contain the same information. Is this intentional? Is this something that could be updated?\n",
    "\n",
    "Kevin K. Pierce responded with a corrected version of the files:\n",
    "\n",
    "> You are correct that the 2005-2006 county-to-county data files on our website are not correct. We will work to correct the files. If you come across any other errors, please let us know. In the meantime, I have attached copies of the files and the corresponding record layouts.\n",
    "\n",
    "\n",
    "## Cleaning the 2011 Dataset\n",
    "\n",
    "I noticed that there seemed to be an error with the 2011 data and sent the following message to SIS@irs.gov:\n",
    "\n",
    "> If you look at the 2011-2012 Outgoing Migration Flow data for Washington County, Kansas (statefips 20, countyfips 201) the number of returns is far larger than the population of the county (~15k returns for ~5k population). In the 2012-2013 data however, the county shows a sensible number of outgoing returns (around 100). Is this a possible error in the data processing? If so would it be possible to get corrected data?\n",
    "\n",
    "The response I recieved from Kevin K. Pierce was:\n",
    "\n",
    "> Thank you for bringing this issue to our attention. It does appear to be an anomaly with the data. Currently, the best solution we can offer is to use the migration figures from the prior and subsequent years for this particular county. We regret this inconvenience.\n",
    "\n",
    "\n",
    "To fix this I cleaned the 2011 countyinflow and countyoutflow data files by hand. I removed all incoming and outgoing rows in the 2011-2012 data (for county id 20201) in which the to/from counties were not also present in the 2012-2013 data (this turned out to be everything but migrations to and from the neighboring Marshall county with county id 20117, as the 2012-2013 data only included migrations to and from Marshall county).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Census Data\n",
    "\n",
    "For the population data requirements we use county by year population estimates from the US Census.\n",
    "\n",
    "## First Set\n",
    "\n",
    "For the 2000-2010 data I am using the County Intercensal Estimates (2000-2010) dataset available for download [here](https://www.census.gov/popest/data/intercensal/county/files/CO-EST00INT-TOT.csv). The information page can be found [here](https://www.census.gov/popest/data/intercensal/county/county2010.html).\n",
    "\n",
    "## Second Set\n",
    "\n",
    "For 2010-2015 data I am using data exported from American Fact Finder which can be obtained by going [here](https://www.census.gov/popest/data/counties/totals/2015/CO-EST2015-01.html) then clicking \"All States\". The information page can be found [here](https://www.census.gov/popest/data/counties/totals/2015/index.html). The name of the dataset to the best of my knowledge is \"Annual Estimates of the Resident Population: April 1, 2010 to July 1, 2015\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Generating C-Value Maps and C-Value Map Diffs\n",
    "\n",
    "I generate 2 series of c-value maps and c-value diff maps: from 2004-2012 and from 2008-2010. This is because Hurricane Katrina throws off the color scale for years outside 2005-2007. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "application/javascript": [
       "$.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')"
      ],
      "text/plain": [
       "<IPython.core.display.Javascript object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%%javascript\n",
    "$.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}

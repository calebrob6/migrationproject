#!/usr/bin/env python
import sys,os
import time
import argparse

def doArgs(argList, name):
    parser = argparse.ArgumentParser(description=name)

    parser.add_argument('-v', "--verbose", action="store_true", help="Enable verbose debugging", default=False)
    parser.add_argument('--input', action="store", dest="input", type=int, help="Year to format", required=True)
    parser.add_argument('--output', action="store", dest="outputFn", type=str, help="Output file name", required=True)

    return parser.parse_args(argList)

def main():
    progName = "Format Census Population Data"
    args = doArgs(sys.argv[1:], progName)

    verbose = args.verbose
    outputFn = args.outputFn
    inputYear = args.input

    print "Starting %s" % (progName)
    startTime = float(time.time())

    outputBase = os.path.dirname(outputFn)
    if outputBase!='' and not os.path.exists(outputBase):
        print "Output directory doesn't exist, making output dirs: %s" % (outputBase)
        os.makedirs(outputBase)


    f = open("data/input/visualizerOutputFormat.csv","r")
    headerLine = f.readline()
    lines = f.read().strip().split("\n")
    f.close()

    data = {}
    for line in lines:
        line = line.strip()
        if line!="":
            parts = line.split(",")
            data[int(parts[0])] = parts

    if inputYear<2010:
        offset = inputYear - 2000

        f = open("data/input/countyPopulation2000_2010.csv","r")
        f.readline()
        lines = f.read().strip().split("\n")
        f.close()

        newData = {}
        for line in lines:
            line = line.strip()
            if line!="":
                parts = line.split(",")
                stateCode = parts[3]
                countyCode = "%03d" % int(parts[4])
                fullCode = int(stateCode+countyCode)

                if fullCode in data:
                    newData[fullCode] = parts[8+offset]
                else:
                    print "Error " + str(fullCode) + " key not found"
                    pass
    else:
        offset = inputYear - 2010

        f = open("data/input/countyPopulation2010_2015.csv","r")
        headerLine = f.readline()
        lines = f.read().strip().split("\n")
        f.close()

        newData = {}
        for line in lines:
            line = line.strip()
            if line!="":
                parts = line.split(",")
                fullCode = int(parts[1])

                if fullCode in data:
                    newData[fullCode] = parts[5+offset]
                else:
                    #print "Error " + str(fullCode) + " key not found"
                    newData[fullCode] = parts[5+offset] # add it anyway
                    pass

        '''
        Data cleaning step

        In newData, but not data 2158
        In newData, but not data 46102

        In data, but not newData 2270
        In data, but not newData 51515
        In data, but not newData 46113
        '''
        newData[2270] = newData[2158]
        newData[46113] = newData[46102]
        newData[51515] = '6243' #last recorded population for this county
        del newData[2158]
        del newData[46102]

    assert len(data) == len(newData)

    keys = sorted(data.keys())
    f = open(outputFn,"w")
    f.write("%d\n" % len(data))
    for key in keys:
        data[key][3] = newData[key]
        f.write("%s\n" % (",".join(data[key])))
    f.close()

    print "Wrote %d records" % (len(data))
    print "Finished in %0.4f seconds" % (time.time() - startTime)
    return

if __name__ == '__main__':
    #sys.argv = ["programName.py","--input","2011","--output","data/output/population/2011_population_data.csv"]
    main()

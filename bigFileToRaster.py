import numpy as np
from osgeo import gdal
from osgeo import osr


# Load elevation dataset
bigImage = np.load("bigImage.npy")

# minimum longitude, minimum latitude, maximum longitude, maximum latitude
xmin, ymin, xmax, ymax = [-180, -60, 180, 90]
#xmin, ymin, xmax, ymax = [-180, -90, 180, 90]


nrows, ncols = np.shape(bigImage)
xres = (xmax - xmin) / float(ncols)
yres = (ymax - ymin) / float(nrows)

geotransform = (xmin, xres, 0, ymax, 0, -yres)

output_raster = gdal.GetDriverByName('GTiff').Create(
    'myraster.tif',
    ncols,
    nrows,
    1,
    gdal.GDT_Float32
)

output_raster.SetGeoTransform(geotransform)
srs = osr.SpatialReference()
srs.ImportFromEPSG(4326)

output_raster.SetProjection(srs.ExportToWkt())
output_raster.GetRasterBand(1).WriteArray(bigImage)

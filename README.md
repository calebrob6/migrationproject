



Data from:

* https://www.census.gov/popest/data/intercensal/county/county2010.html
* https://www.irs.gov/uac/SOI-Tax-Stats-Migration-Data

Visualizer based on the work done by Patrick Renschler, see https://github.com/renschler/radiationmodel

---

To generate the output data:

1. ./makePopulationOutput.sh
2. ./runCValTests.sh
3. ./visRadiation2008.sh

---

[Simini et al. (2012)](http://www.nature.com/nature/journal/v484/n7392/abs/nature10856.html) present a model for determining fluxes between communities that only requires a population distribution as an input. This model, coined the [radiation model](http://en.wikipedia.org/wiki/Radiation_law_for_human_mobility), has been shown to improve the accuracy of describing many processes that are affected by mobility and transport including migration, trade, and communication.

Here we can visualize the output of a radiation model that was used to calculate commuter flows between counties in the United States. These flows were generated using 2010 Census data [(radiation model source code)](https://github.com/renschler/radiationmodel). The selected county appears <span style="color:red">red</span>. You can toggle between commuter flows into and out of the county.

Although there are limitations to the results it's cool to see that this basic radiation model captures some long-distance movement patterns that other mobility predictors (like the [gravity model](http://en.wikipedia.org/wiki/Gravity_model_of_trade)) struggle with in the absence of data needed to fit parameters. For good examples check out Miami-Dade, Florida; Cook, Illinois; and Clark, Nevada.

As a future project I'd be interested in applying a radiation model to investigate the dynamics of human mobility and disease transmission across nations where accurate mobility data is not always available.

#!/usr/bin/env python
import sys,os
import time
import argparse

import numpy as np
from haversine import haversine
import scipy.spatial

def doArgs(argList, name):
    parser = argparse.ArgumentParser(description=name)

    parser.add_argument('-v', "--verbose", action="store_true", help="Enable verbose debugging", default=False)
    parser.add_argument('--input', action="store", dest="inputFn", type=str, help="Input file name, csv where first line is line count, the rest should be formated like so (1001,Autauga,Alabama,54571,32.500389,-86.494165)", required=True)
    parser.add_argument('--output', action="store", dest="outputFn", type=str, help="Output file name for the OD matrix with migration counts", required=True)
    parser.add_argument('--c', action="store", dest="cRate", type=float, help="Migration fraction", required=False, default=0.10)
    return parser.parse_args(argList)

def main():
    progName = "Radiation Model"
    args = doArgs(sys.argv[1:], progName)

    verbose = args.verbose
    inputFn = args.inputFn
    outputFn = args.outputFn
    cRate = args.cRate

    print "Starting %s" % (progName)
    startTime = float(time.time())

    if not os.path.isfile(inputFn):
        print "Input doesn't exist, exiting"
        return

    outputBase = os.path.dirname(outputFn)
    if outputBase!='' and not os.path.exists(outputBase):
        print "Output directory doesn't exist, making output dirs: %s" % (outputBase)
        os.makedirs(outputBase)

    #------------------------------------------------------------------------------------
    '''
    Read in the input file
    '''
    print "Loading input from: %s" % (inputFn)
    f = open(inputFn,"r")
    f.readline()
    lines = f.read().strip().split("\n")
    f.close()

    data = {}
    totalPopulation = 0
    for line in lines:
        line = line.strip()
        if line!="":
            parts = line.split(",")
            countyPopulation = int(parts[3])
            totalPopulation += countyPopulation
            data[int(parts[0])] = [countyPopulation] + parts


    n = len(data)
    keys = sorted(data.keys()) #we use this to iterate over the dictionary to make sure we go through it the same way each time.

    #here we build the matrix to hold our output
    odMatrix = dict()
    for key in keys:
        odMatrix[key] = dict()


    #------------------------------------------------------------------------------------
    '''
    Here we calculate the n*n county distance matrix using the haversine formula.
    We cache the results to file so we don't have to recompute this at runtime all the time (this saves ~30 seconds on my machine)
    '''
    if not os.path.exists("outputDistanceMatrix.npy"):
        print "Calculating distance matrix"
        coords = [(float(data[k][5]),float(data[k][6])) for k in keys]
        distanceMatrix = scipy.spatial.distance.cdist(coords,coords,haversine)
        #np.savetxt("outputDistanceMatrix.csv", distanceMatrix, fmt='%f', delimiter=',', newline='\n')
        np.save("outputDistanceMatrix.npy",distanceMatrix)
    else:
        print "Loading the distance matrix"
        #distanceMatrix = np.loadtxt("outputDistanceMatrix.csv", dtype=float, delimiter=",")
        distanceMatrix = np.load("outputDistanceMatrix.npy")


    #------------------------------------------------------------------------------------
    '''
    Fill in the radiation model calculations. The process is as follows:
        1.) For a given county, k, we use the the distanceMatrix to sort the other counties by distance (from least to greatest)
        2.) Loop through the sorted list of other counties and apply the radiation model formula to fill in row k of the odMatrix output
        3.) We use S as the s_ij term, this will increase in each iteration by the previous county's population because we sorted other counties by distance
    '''
    modelStartTime = float(time.time())
    print "Running radiation model"
    for i,k in enumerate(keys):
        if i%100==0 and verbose:
            print "%d/%d\t%0.4f" % (i,n,time.time()-modelStartTime)

        currentPopulation = float(data[k][0])
        #norm = totalPopulation/(totalPopulation-currentPopulation)

        #sort other counties by distance (closest to farthest away)
        otherPatches = []
        for j,k2 in enumerate(keys):
            otherPatches.append((distanceMatrix[i][j],k2))
        otherPatches.sort(key=lambda x:x[0])

        S = 0
        for distance,p in otherPatches:
            otherPatch = data[p]
            otherPopulation = float(otherPatch[0])

            #num = norm*currentPopulation*cRate*currentPopulation*otherPopulation
            num = currentPopulation*cRate*currentPopulation*otherPopulation
            denom = (currentPopulation+S)*(currentPopulation+S+otherPopulation)
            total = num/denom

            odMatrix[k][p] = int(total)
            S += otherPopulation #after we are done with an iteration, the population of the county we are looking at will be included in the s_ij term for the next farthest away county


    #------------------------------------------------------------------------------------
    '''
    We can either output the entire odMatrix to a csv file, or create a ton of inflow, outflow files for the visualizer
    '''
    print "Finished running model, writing output to: %s" % (outputFn)
    SINGLE_FILE_OUTPUT = True
    if SINGLE_FILE_OUTPUT:
        #Single OD matrix with header line for county ID's
        f = open(outputFn,"w")
        headerLine = ','.join(map(str,keys))
        f.write("%s\n"%(headerLine))
        for lineNum,i in enumerate(keys):
            outputLine = []
            for j in keys:
                outputLine.append(odMatrix[i][j])
            assert len(outputLine)==n
            f.write("%s\n" % (','.join(map(str,outputLine))))
        f.close()
    else:
        #For the visualizer
        countyOutPath = os.path.join(outputBase,"county_outflows/")
        if not os.path.exists(countyOutPath):
            os.makedirs(countyOutPath)

        countyInPath = os.path.join(outputBase,"county_inflows/")
        if not os.path.exists(countyInPath):
            os.makedirs(countyInPath)

        for key in keys:
            countyIn = open(os.path.join(countyInPath,"%s.csv"%(key)),"w")
            countyOut = open(os.path.join(countyOutPath,"%s.csv"%(key)),"w")

            countyIn.write("id,commuters\n")
            countyOut.write("id,commuters\n")

            for key2 in keys:
                countyIn.write("%s,%d\n" % (key2,odMatrix[key2][key]))
                countyOut.write("%s,%d\n" % (key2,odMatrix[key][key2]))

            countyIn.close()
            countyOut.close()

    print "Finished in %0.4f seconds" % (time.time() - startTime)
    return

if __name__ == '__main__':
    main()
